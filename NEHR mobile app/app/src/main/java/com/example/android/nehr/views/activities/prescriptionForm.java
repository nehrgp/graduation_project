package com.example.android.nehr.views.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.models.PrescriptionModel;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.android.nehr.sessions.SaveSharedPreference.*;

public class prescriptionForm extends NavigationDrawer {
    @BindView(R.id.doctorNameTxt) EditText doctorNameTxt;
    @BindView(R.id.presDateTxt) TextView presDateTxt;
    @BindView(R.id.saveBtn) Button saveBtn;
    @BindView(R.id.cameraBtn) Button cameraBtn ;
    @BindView(R.id.imgPreview) ImageView imagePreview ;
    ArrayList<PrescriptionModel> prescriptionModelsList = new ArrayList<PrescriptionModel>() ;
    Gson gson = new Gson();
    String prescriptionModelsListJson;
    String  imageString ;
    int day;
    int month;
    int year;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "prescriptions";
    private Uri fileUri;
    PrescriptionModel prescriptionModel ;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_form);
        ButterKnife.bind(this);
        prescriptionModel = new PrescriptionModel();
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
            finish();
        }


        presDateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        prescriptionForm.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                presDateTxt.setText(date);
            }
        };
    }

    @OnClick({R.id.cameraBtn})
    public void cameraBtnClicked(){
        captureImage();
    }
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @OnClick(R.id.saveBtn)
    public void saveBtnClicked(){
        if (isValid()) {
            prescriptionModel.setDoctorName(String.valueOf(doctorNameTxt.getText()));
            prescriptionModel.setPrescriptionDate(String.valueOf(presDateTxt.getText()));

            if(SaveSharedPreference.getPresList(getApplicationContext()).length() == 0){
                prescriptionModelsList.add(prescriptionModel);
                prescriptionModelsListJson = gson.toJson(prescriptionModelsList);
                SaveSharedPreference.setPresList(getApplicationContext(), prescriptionModelsListJson);
            }
            else{
                prescriptionModelsListJson = SaveSharedPreference.getPresList(getApplicationContext());
                prescriptionModelsList = gson.fromJson(prescriptionModelsListJson, new TypeToken<List<PrescriptionModel>>() {
                }.getType());

                prescriptionModelsList.add(prescriptionModel);
                prescriptionModelsListJson = gson.toJson(prescriptionModelsList);
                SaveSharedPreference.setPresList(getApplicationContext(), prescriptionModelsListJson);
            }

            Intent i = new Intent(prescriptionForm.this, myPrescriptions.class);
            startActivity(i);
            finish();
        }
    }

    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void previewCapturedImage() {
        try {
            imagePreview.setVisibility(View.VISIBLE);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 5;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);
            imagePreview.setImageBitmap(bitmap);
            imageString = getStringFromBitmap(bitmap);
            prescriptionModel.setPrescriptionImage(imageString);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private String  getStringFromBitmap(Bitmap bitmapPicture) {
        /*
         * This functions converts Bitmap picture to a string which can be
         * JSONified.
         * */
        final int COMPRESSION_QUALITY = 100;
        //String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    private boolean isValid() {
        boolean valid = true;
        if (doctorNameTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter doctor name", Toast.LENGTH_SHORT).show();
        }
       /* if (presTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter prescription date", Toast.LENGTH_SHORT).show();
        }*/
        return valid;
    }
}
