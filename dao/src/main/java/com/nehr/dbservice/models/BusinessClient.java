package com.nehr.dbservice.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "businessClient")
public class BusinessClient {

	String userName;
	String businessClientType;
	String email;
	String passwordHash;
	
	
	@DynamoDBAttribute(attributeName = "userName")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@DynamoDBAttribute(attributeName = "businessClientType")
	public String getBusinessClientType() {
		return businessClientType;
	}
	public void setBusinessClientType(String businessClientType) {
		this.businessClientType = businessClientType;
	}
	
	@DynamoDBHashKey(attributeName = "email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@DynamoDBAttribute(attributeName = "passwordHash")
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
}
