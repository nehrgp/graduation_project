package com.nehr.dbservice.models;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "patient")
public class PatientMedicalHistory {

	private String nID;
	private Integer length;
	private Integer weight;
	private Boolean smoker;
	private Boolean alcoholic;
	private Boolean pregnant;
    private List<String> allergies;

	@DynamoDBHashKey(attributeName = "nID")
	public String getnID() {
		return nID;
	}
	public void setnID(String nID) {
		this.nID = nID;
	}
	
	
	@DynamoDBAttribute(attributeName = "length")
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	
	
	@DynamoDBAttribute(attributeName = "weight")
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@DynamoDBAttribute(attributeName = "smoker")
	public Boolean getSmoker() {
		return smoker;
	}
	public void setSmoker(Boolean smoker) {
		this.smoker = smoker;
	}
	
	@DynamoDBAttribute(attributeName = "alcoholic")
	public Boolean getAlcoholic() {
		return alcoholic;
	}
	public void setAlcoholic(Boolean alcoholic) {
		this.alcoholic = alcoholic;
	}
	
	@DynamoDBAttribute(attributeName = "pregnant")
	public Boolean getPregnant() {
		return pregnant;
	}
	public void setPregnant(Boolean pregnant) {
		this.pregnant = pregnant;
	}
	
	
	@DynamoDBAttribute(attributeName = "allergies")
	public List<String> getAllergies() {
		return allergies;
	}
	public void setAllergies(List<String> allergies) {
		this.allergies = allergies;
	}
	    
    
}
