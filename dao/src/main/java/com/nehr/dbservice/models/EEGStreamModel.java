package com.nehr.dbservice.models;

import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "EEGStream")
public class EEGStreamModel {

	String id;
	ArrayList<ArrayList<Float>> data;
	
	public EEGStreamModel() {
		// TODO Auto-generated constructor stub
	}
	
	@DynamoDBHashKey(attributeName = "id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@DynamoDBAttribute(attributeName = "data")
	public ArrayList<ArrayList<Float>> getData() {
		return data;
	}

	public void setData(ArrayList<ArrayList<Float>> data) {
		this.data = data;
	}

}
