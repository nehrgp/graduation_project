package com.example.android.nehr.data;

import android.os.AsyncTask;
import android.util.Log;

import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.presenters.DrugSearchPresenter;
import com.example.android.nehr.presenters.LoginPresenter;
import com.example.android.nehr.views.views.DrugSearchView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by omar on 2/3/2018.
 */

public class Search extends AsyncTask<Void,Void,DrugModel> {

    DrugModel drugModel;
    DrugSearchPresenter presenter;

    public Search(DrugSearchPresenter presenter,String name ) {
        this.drugModel = drugModel;
        this.presenter = presenter;
    }

    @Override
    protected DrugModel doInBackground(Void... voids) {
        try {
            final String url = DataConfig.getDbServiceUrl()+"/drug/find/prefix/" + "asprim";
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            DrugModel drugModel = restTemplate.getForObject(url, DrugModel.class);
            return drugModel;
        } catch (Exception e) {
            Log.e("loginActivity", e.getMessage(), e);
        }
        return null;
    }



    protected void onPostExecute(List<DrugModel> returnedDrugModel) {

        if(returnedDrugModel!=null ){
            presenter.searchSuccess(returnedDrugModel);
            Log.d("ourMsg","onSuccess");

        }
        else{
            Log.d("ourMsg","onFailed");

        }
        //presenter will change the ui according to the returned data
    }
}