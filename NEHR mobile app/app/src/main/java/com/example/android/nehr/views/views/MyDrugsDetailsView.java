package com.example.android.nehr.views.views;

/**
 * Created by omar on 2/11/2018.
 */

public interface MyDrugsDetailsView extends BaseView {
    public void drugShowSuccess();
    public void drugShowFailed();
}
