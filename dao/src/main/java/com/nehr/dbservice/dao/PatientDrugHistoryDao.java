package com.nehr.dbservice.dao;

import java.util.List;

import com.nehr.dbservice.models.DrugHistory;
import com.nehr.dbservice.models.PatientDrugHistory;

public interface PatientDrugHistoryDao {

	PatientDrugHistory insertDrug (PatientDrugHistory patientDrugHistory);
	void deleteDrugByName(String patientId ,String drugName);
	PatientDrugHistory findPatientDrugHistory(String patientId);
	boolean  contradict(String drug1,String drug2);
}
