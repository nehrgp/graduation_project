package com.nehr.dbservice.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.nehr.dbservice.models.Drug;

@Repository
public class DrugDaoImpl implements DrugCrudDao, DrugGlobalOperationsDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;

	@Override
	public Drug upsertDrug(Drug drug) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		drug.setName(drug.getName().toLowerCase());
		try {
			mapper.save(drug, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return findDrugByName(drug.getName());
		}

	}

	@Override
	public Drug findDrugById(String id) {

		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		Drug drug = new Drug();
		drug.setId(id);
		Drug result = null;
		try {
			result = mapper.load(drug);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;

		}
	}

	@Override
	public Drug findDrugByName(String name) {

		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		HashMap<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":v1", new AttributeValue().withS(name));
		HashMap<String, String> expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeNames.put("#name", "name");
		DynamoDBQueryExpression<Drug> queryExpression = new DynamoDBQueryExpression<Drug>().withIndexName("name-index")
				.withConsistentRead(false).withKeyConditionExpression("#name = :v1").withExpressionAttributeValues(eav)
				.withExpressionAttributeNames(expressionAttributeNames);
		List<Drug> iList = mapper.query(Drug.class, queryExpression);
		if (iList.isEmpty()) {
			return null;
		} else
			return iList.get(0);
	}

	@Override
	public void deleteDrugById(String id) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		Drug drug = new Drug();
		drug.setId(id);
		mapper.delete(drug);

	}

	@Override
	public List<Drug> findAll() {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		return mapper.scan(Drug.class, new DynamoDBScanExpression());
	}

	@Override
	public void deActivateDrug(String id) {
		Drug drug = new Drug();
		drug.setId(id);
		drug.setActive(false);
		this.upsertDrug(drug);

	}

	@Override
	public List<Drug> getDrugsByPrefix(String prefix) {

		return this.getDrugsUsingContainFilter("name", prefix);
	}

	@Override
	public List<Drug> getDrugsByEffectiveMaterial(String effectiveMaterial) {

		return this.getDrugsUsingContainFilter("effectiveSubstance", effectiveMaterial);

	}

	@Override
	public List<Drug> getDrugsBySymptom(String symptom) {
		return this.getDrugsUsingContainFilter("symptoms", symptom);
	}

	@Override
	public List<Drug> getDrugsByDisease(String disease) {
		return this.getDrugsUsingContainFilter("diseases", disease);

	}

	private List<Drug> getDrugsUsingContainFilter(String attributeName, String attributeValue) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		HashMap<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(attributeValue));
		HashMap<String, String> expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeNames.put("#att1", attributeName);

		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
				.withFilterExpression("contains(#att1, :val1)").withExpressionAttributeValues(eav)
				.withExpressionAttributeNames(expressionAttributeNames);
		List<Drug> drugs = mapper.scan(Drug.class, scanExpression);

		return drugs;
	}

}
