package com.example.android.nehr.interactors;

import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.presenters.DrugDetailsPresenter;
import com.example.android.nehr.presenters.DrugPresenter;
import com.example.android.nehr.presenters.DrugSearchPresenter;
import com.example.android.nehr.presenters.MyDrugHistoryDetailsPresenter;

/**
 * Created by omar on 2/4/2018.
 */

public interface DrugInteractor {
    void callSearchAPI(DrugSearchPresenter presenter , String name);
    void callAddDrugAPI(DrugDetailsPresenter presenter , PatientDrugHistory patientDrugHistory);
    void callAddDrugHistoryAPI(MyDrugHistoryDetailsPresenter presenter , PatientDrugHistory patientDrugHistory);
    void callGetDrugAPI(DrugPresenter presenter, String id);

}
