package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.android.nehr.R;
import com.example.android.nehr.data.Search;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.presenters.DrugSearchPresenter;
import com.example.android.nehr.presenters.DrugSearchPresenterImpl;
import com.example.android.nehr.views.adapters.DrugListAdapter;
import com.example.android.nehr.views.views.DrugSearchView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class alldrugsActivity extends NavigationDrawer implements DrugSearchView , SearchView.OnQueryTextListener {

    //@BindView(R.id.drugs_names) TextView drugNames ;
    //@BindView(R.id.toolbar) Toolbar mToolbar ;
    @BindView(R.id.drugListView) ListView listView;
    @BindView(R.id.search) SearchView searchView ;

    DrugModel[] drugArray;
    DrugModel drugModel = new DrugModel();
    DrugSearchPresenter presenter = new DrugSearchPresenterImpl(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alldrugs);
        ButterKnife.bind(this);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Search drug name");

    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String prefix) {
               // Log.d("ourMsg", "onQuerySubmitCalled");
               // presenter.attemptSearch(prefix);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String prefix) {
                presenter.attemptSearch(prefix);
                return false;
            }
        });
        return true;
    }*/


/*

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.startActivity(new Intent(this, dashboardActivity.class));

        }
        return true;
    }*/

    @Override
    public void showDrugs(List<DrugModel> drugs) {

      // DrugModel[] drugArray = new DrugModel[drugs.size()];
     //  drugs.toArray(drugArray);

        DrugListAdapter adapter = new DrugListAdapter(this,drugs);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),drugDetails.class);
                drugModel.setName(adapter.getItem(position).getName());
                drugModel.setDescription(adapter.getItem(position).getDescription());
                drugModel.setDiseases(adapter.getItem(position).getDiseases());
                drugModel.setEffectiveSubstance(adapter.getItem(position).getEffectiveSubstance());
                drugModel.setSymptoms(adapter.getItem(position).getSymptoms());
                drugModel.setActive(adapter.getItem(position).isActive());
                drugModel.setImage(adapter.getItem(position).getImage());
                intent.putExtra("drugModel",drugModel);
                startActivity(intent);
            }
        });
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String prefix) {
        presenter.attemptSearch(prefix);
        return false;
    }
}
