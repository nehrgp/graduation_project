package com.example.android.nehr.interactors;

import android.util.Log;

import com.example.android.nehr.data.DBClient;
import com.example.android.nehr.data.DBClientImpl;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.presenters.DrugDetailsPresenter;
import com.example.android.nehr.presenters.DrugPresenter;
import com.example.android.nehr.presenters.DrugSearchPresenter;
import com.example.android.nehr.presenters.MyDrugHistoryDetailsPresenter;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by omar on 2/4/2018.
 */

public class DrugInteractorImpl implements DrugInteractor {
    DBClient dbClient;
    Observer<List<DrugModel>> drugModelObserver;
    Observer<PatientDrugHistory> patientDrugHistoryObserver;
    Observer<PatientDrugHistory> patientDrugHistoryObserver2;
    Observer<List<DrugHistory>> drugHistoryObserver;


    public DrugInteractorImpl( ) {
        this.dbClient = new DBClientImpl();
    }



    @Override
    public void callSearchAPI(DrugSearchPresenter presenter, String prefix) {
        drugModelObserver =new Observer<List<DrugModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<DrugModel> returnedDrugList) {
                if (returnedDrugList != null ) {
                    presenter.searchSuccess(returnedDrugList);

                } else {
                    presenter.searchFailure();

                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d("ourMsg", "maybeNetworkError");

            }

            @Override
            public void onComplete() {

            }
        };
        dbClient.getDrugsByPrefix(prefix)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(drugModelObserver);
    }

    @Override
    public void callAddDrugAPI(DrugDetailsPresenter presenter, PatientDrugHistory patientDrugHistory) {
        patientDrugHistoryObserver =new Observer<PatientDrugHistory>() {
            @Override
            public void onSubscribe(Disposable d) { }
            @Override
            public void onNext(PatientDrugHistory patientDrugHistory) {
                if (patientDrugHistory != null ) {
                    presenter.drugAddedSuccesfully();
                } else {
                    presenter.drugAddingFailed();
                }
            }
            @Override
            public void onError(Throwable e) {
                Log.d("ourMsg", "maybeNetworkError");
                presenter.drugAddingFailed();
            }
            @Override
            public void onComplete() {
            }
        };
        dbClient.addDrugToPatientDrugHistory(patientDrugHistory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(patientDrugHistoryObserver);
    }

    @Override
    public void callAddDrugHistoryAPI(MyDrugHistoryDetailsPresenter presenter, PatientDrugHistory patientDrugHistory) {
        patientDrugHistoryObserver2 =new Observer<PatientDrugHistory>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(PatientDrugHistory patientDrugHistory) {
                if (patientDrugHistory != null ) {
                    presenter.drugAddedSuccesfully();

                } else {
                    presenter.drugAddingFailed();

                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d("ourMsg", "patientDrugHistoryError");
                Log.e("Error",e.getMessage());
                presenter.drugAddingFailed();

            }

            @Override
            public void onComplete() {

            }
        };
        dbClient.addDrugToPatientDrugHistory(patientDrugHistory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(patientDrugHistoryObserver2);
    }

    @Override
    public void callGetDrugAPI(DrugPresenter presenter, String id) {
       drugHistoryObserver = new Observer<List<DrugHistory>>() {
           @Override
           public void onSubscribe(Disposable d) {

           }

           @Override
           public void onNext(List<DrugHistory> returnedDrugHistory) {

               if (returnedDrugHistory != null ) {
                   presenter.success(returnedDrugHistory);

               } else {
                   presenter.fail();

               }
           }

           @Override
           public void onError(Throwable e) {
               Log.d("ourMsg", "myDrugsError");
           }

           @Override
           public void onComplete() {

           }
       };
        dbClient.getPatientDrugs(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(drugHistoryObserver);

    }



}
