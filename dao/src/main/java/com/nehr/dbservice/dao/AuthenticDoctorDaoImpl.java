package com.nehr.dbservice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.nehr.dbservice.models.AuthenticDoctor;

@Repository
public class AuthenticDoctorDaoImpl implements AuthenticDoctorDao{

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;
	
	@Override
	public Boolean isAuthentic(String patientID, String doctorEmail) {
		AuthenticDoctor authenticDoctor = this.findPatientID(doctorEmail);
		if(authenticDoctor == null) {
			return false;
		} else if (authenticDoctor.getPatientID().contains(patientID)){
			return true;
		} else {
			return false;
		}
	}

	@Override
	public AuthenticDoctor findPatientID(String doctorEmail) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		AuthenticDoctor authenticDoctor = new AuthenticDoctor();
		authenticDoctor.setDoctorEmail(doctorEmail);
		AuthenticDoctor result = null;
		try {
			result = mapper.load(authenticDoctor);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	@Override
	public AuthenticDoctor addPatientID(AuthenticDoctor authenticDoctor) {
		
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		
		AuthenticDoctor updatedAuthenticDoctor = new AuthenticDoctor();
		
		if(this.findPatientID(authenticDoctor.getDoctorEmail()) == null) {
			updatedAuthenticDoctor = this.findPatientID(authenticDoctor.getDoctorEmail());
		}
		
		try {
			updatedAuthenticDoctor.setDoctorEmail(authenticDoctor.getDoctorEmail());
			updatedAuthenticDoctor.getPatientID().addAll(authenticDoctor.getPatientID());
		} catch(RuntimeException e) {
			System.out.println("catch");
			updatedAuthenticDoctor = authenticDoctor;
		}
		/*
		if(updatedAuthenticDoctor.getDoctor_email() == null) {
			updatedAuthenticDoctor = authenticDoctor;
		} else {
			updatedAuthenticDoctor.setPatientID(authenticDoctor.getPatientID());
			updatedAuthenticDoctor.getDoctor_email().addAll(authenticDoctor.getDoctor_email());
		}
		*/
		try {
			mapper.save(updatedAuthenticDoctor, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return findPatientID(updatedAuthenticDoctor.getDoctorEmail());
		}
	}
}
