package com.nehr.dbservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.PrescriptionDao;
import com.nehr.dbservice.models.Prescription;

@RestController
@RequestMapping("/prescription")
public class PrescriptionController {

	@Autowired
	PrescriptionDao prescriptionDao;
	
	
	@PostMapping("/addPrescription")
	public Prescription addPrescription(@RequestBody Prescription prescription) {
		return prescriptionDao.addPrescription(prescription);
	}
	
	@GetMapping("/findPrescription/{patient_nId}")
	public List<Prescription> findPrescription(@PathVariable String patient_nId){
		return prescriptionDao.findPrescriptionById(patient_nId);
	}
}
