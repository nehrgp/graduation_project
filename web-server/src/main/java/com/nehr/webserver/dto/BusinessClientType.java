package com.nehr.webserver.dto;

public enum BusinessClientType {
	DOCTOR("طبيب"),
	PHARMACY("صيدلية"),
	INSURANCE_COMPANY("شركة تأمين"),
	PHARMA_COMPANY("شركة أدوية"),
	GOVERNMET_AGENCY("مؤسسة حكومية"),
	OTHERS("أخرى");
	
    private final String displayName;

    BusinessClientType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
