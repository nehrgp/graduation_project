package com.nehr.dbservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.BusinessClientDao;
import com.nehr.dbservice.models.BusinessClient;

@RestController
@RequestMapping("/businessclient")
public class BusinessClientController {

	@Autowired 
	BusinessClientDao BussinessClientDao;
	
    @RequestMapping(value="/upsert",method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public BusinessClient upsertDrug(@RequestBody BusinessClient businessClient){
    	return BussinessClientDao.upsertBusinessClient(businessClient);
    }
    
    @RequestMapping(value="/delete/{email}",method=RequestMethod.DELETE)
    public void deleteDrugById(@PathVariable("email") String email){
    	BussinessClientDao.deleteBusinessClientByuserName(email);
    }
    
    @RequestMapping(value="/find/email/{email}",method=RequestMethod.GET)
    public BusinessClient findBusinessClientByEmail(@PathVariable("email") String email){
    	 return BussinessClientDao.findBusinessClientByEmail(email);
    }
    
    @RequestMapping(value="/find/all")
    public List<BusinessClient> findAll(){
    	return BussinessClientDao.findAll();
    }
}
