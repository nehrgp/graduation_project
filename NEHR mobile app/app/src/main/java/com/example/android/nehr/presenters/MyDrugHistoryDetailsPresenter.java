package com.example.android.nehr.presenters;

import com.example.android.nehr.models.DrugHistory;

/**
 * Created by omar on 2/15/2018.
 */

public interface MyDrugHistoryDetailsPresenter {
    public void addToMyDrugs(DrugHistory drugHistory);
    public void drugAddedSuccesfully();
    public void drugAddingFailed();
}
