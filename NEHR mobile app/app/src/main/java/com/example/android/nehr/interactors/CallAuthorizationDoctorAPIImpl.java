package com.example.android.nehr.interactors;

import android.util.Log;

import com.example.android.nehr.data.DBClient;
import com.example.android.nehr.data.DBClientImpl;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CallAuthorizationDoctorAPIImpl implements CallAuthorizationDoctorAPI {
    DBClient dbClient;
    Observer<String> doctorAuthorizationObserver;

    public CallAuthorizationDoctorAPIImpl(){
        this.dbClient = new DBClientImpl();
    }
    @Override
    public void callLoginAPI(String response, String doctorEmail, String patientNID) {
        doctorAuthorizationObserver = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String strings) {

            }

            @Override
            public void onError(Throwable e) {
                Log.d("myError","Error in CallAuthorizationDoctorAPIImpl");
                Log.d("Error:",e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
        dbClient.sendAuthorizationResponse(response,doctorEmail,patientNID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(doctorAuthorizationObserver);
    }
}
