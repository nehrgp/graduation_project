package com.amazonaws.lambda.demo;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;

import java.awt.List;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Spring;

import com.amazon.redshift.jdbc.Driver;


public class LambdaFunctionHandler implements RequestHandler<DynamodbEvent, Integer> {
	static final String JDBC_DRIVER = "https://s3.amazonaws.com/redshift-downloads/drivers/jdbc/1.2.12.1017/RedshiftJDBC42-1.2.12.1017.jar";  
	static final String DB_URL = "jdbc:redshift://data-warehouse.copyte5s6ypv.us-east-2.redshift.amazonaws.com:5439/dev";
	static final String USER = "hossam26644";
	static final String PASS = "4176532Ab";
	
    @Override
    public Integer handleRequest(DynamodbEvent event, Context context) {
        context.getLogger().log("Received event: " + event);

        for (DynamodbStreamRecord record : event.getRecords()) {
            context.getLogger().log(record.getEventID());
            context.getLogger().log(record.getEventName());
            context.getLogger().log(record.getDynamodb().toString());
            
            String EventName = record.getEventName().toString();
            Map<String, AttributeValue> NewImage = record.getDynamodb().getNewImage();
            

            ArrayList<String> Keys = new ArrayList<String>();
            ArrayList<String> Values = new ArrayList<String>();
            
            if (EventName.equals("INSERT") ) {	
        		 for(String key: NewImage.keySet()) {
        			 AttributeValue item = NewImage.get(key);
        			 Keys.add(key);
        			 
        			 if (item.getS()==null) {
        				 if(item.getL()==null) {
        					 Values.add((item.getN()).toString());
            				 System.out.println("Number");
        				 			}
        				 else {
        					 java.util.List<AttributeValue> elements = item.getL();
        					 String SmallValues ="";
        					 for(AttributeValue element:elements) {
        						SmallValues = SmallValues + element.getS();
        					 }
        					 Values.add("'"+SmallValues.toString()+"'");
            				 System.out.println("List");


        				 }
        			 }
        			 else {
        				 String Value = "'" + item.getS()+"'"; 
        				 System.out.println("String");

        				 Values.add(Value);}

        		 }
        		 this.executeQuery(Keys, Values, "druganalytics");
            }

        }
       // this.executeQuery();
        return event.getRecords().size();
    }
    
    
    
    public void executeQuery(ArrayList<String>Keys, ArrayList<String>Values,String TableName  ) {
		Connection conn = null;
		Statement stmt = null;
		System.out.println("Connecting to database...");
		
		String JoinedKeys = String.join(",", Keys);
		String JoinedValues = String.join(",", Values);
		System.out.println(JoinedKeys);
		System.out.println(JoinedValues);
		String Query;
		Query = "INSERT INTO "+TableName+" ("+JoinedKeys+") Values ("+JoinedValues+")";
		//System.out.println(Query);


		try{
		  //STEP 2: Register JDBC driver
		  Class.forName("com.amazon.redshift.jdbc.Driver");

		  //STEP 3: Open a connection
		  System.out.println("Connecting to database...");
		  conn = DriverManager.getConnection(DB_URL,USER,PASS);

		  //STEP 4: Execute a query
		  System.out.println(Query);
		  stmt = conn.createStatement();
		  stmt.executeQuery(Query);

		  
		  stmt.close();
		  conn.close();
		}catch(SQLException se){
		  //Handle errors for JDBC
			System.out.println("hahahahaha");
			System.out.println(se.getMessage() + "hahahahhaha");
		  se.printStackTrace();
		}catch(Exception e){
		  //Handle errors for Class.forName
		  e.printStackTrace();
		}finally{
		  //finally block used to close resources
		  try{
		     if(stmt!=null)
		        stmt.close();
		  }catch(SQLException se2){
		  }// nothing we can do
		  try{
		     if(conn!=null)
		        conn.close();
		  }catch(SQLException se){
		     se.printStackTrace();
		  }//end finally try
		}//end try
		System.out.println("Goodbye!");
}
}