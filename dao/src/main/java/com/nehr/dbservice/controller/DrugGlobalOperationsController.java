package com.nehr.dbservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.DrugGlobalOperationsDao;
import com.nehr.dbservice.models.Drug;

@RestController
public class DrugGlobalOperationsController {

	@Autowired
	DrugGlobalOperationsDao drugGlobalOperationsDao;

	@RequestMapping(value = "/drug/deactivate/{id}", method = RequestMethod.GET)
	public void deactivateDrug(@PathVariable("id") String id) {
		drugGlobalOperationsDao.deActivateDrug(id);
	}

	@RequestMapping(value = "/drug/find/prefix/{prefix}", method = RequestMethod.GET)
	public List<Drug> getDrugsByPrefix(@PathVariable("prefix") String prefix) {
		return drugGlobalOperationsDao.getDrugsByPrefix(prefix);
	}

	@RequestMapping(value = "/drug/find/effective-substance/{effectiveMaterial}", method = RequestMethod.GET)
	public List<Drug> getDrugsByEffectiveMaterial(@PathVariable("effectiveMaterial") String effectiveMaterial) {
		return drugGlobalOperationsDao.getDrugsByEffectiveMaterial(effectiveMaterial);
	}

	@RequestMapping(value = "/drug/find/symptom/{symptom}", method = RequestMethod.GET)
	public List<Drug> getDrugsBySymptom(@PathVariable("symptom") String symptom) {
		return drugGlobalOperationsDao.getDrugsBySymptom(symptom);
	}

	@RequestMapping(value = "/drug/find/disease/{disease}", method = RequestMethod.GET)
	public List<Drug> getDrugsByDisease(@PathVariable("disease") String disease) {
		return drugGlobalOperationsDao.getDrugsByDisease(disease);
	}

}
