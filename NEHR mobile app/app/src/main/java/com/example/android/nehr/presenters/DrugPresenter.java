package com.example.android.nehr.presenters;

import com.example.android.nehr.models.DrugHistory;

import java.util.List;

public interface DrugPresenter {
    public void getMyDrugs();
    public void success(List<DrugHistory> drugHistory);
    public void fail();
}
