import xlrd
import xlutils.copy

import xlwt



workbook = xlrd.open_workbook("DrugsSymbtoms.xls")
outBook = xlutils.copy.copy(workbook)
WorkSheet = workbook.sheet_by_index(0)
RowNumber =  WorkSheet.nrows

wb = xlwt.Workbook() #Creat a new workbook
ws = wb.add_sheet('drugs')
counter = 0
for i in range(1,RowNumber):
	Drugs = WorkSheet.cell(i,1).value
	Drugs = Drugs.splitlines()
	for j in range(0,len(Drugs)):
		ws.write(counter,0,Drugs[j])
		ws.write(counter,1,WorkSheet.cell(i,0).value)
		counter +=1

wb.save("DrugsDestination.xls") 
 
