package com.nehr.webserver.dbmodels;

import java.util.List;



public class PatientDrugHistory {

	private String nID;
	private List<DrugHistory> drugs;
	
	public String getnId() {
		return nID;
	}

	public void setnId(String nID) {
		this.nID = nID;
	}

	public List<DrugHistory> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<DrugHistory> drugs) {
		this.drugs = drugs;
	}
	
	
}

