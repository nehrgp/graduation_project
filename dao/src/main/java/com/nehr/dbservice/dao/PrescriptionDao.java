package com.nehr.dbservice.dao;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.nehr.dbservice.models.Prescription;

public interface PrescriptionDao {

	public Prescription addPrescription(Prescription prescription);
	public Prescription findPrescriptionByUrl(String url);
	public List<Prescription> findPrescriptionById(String nId);
}
