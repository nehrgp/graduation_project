package com.nehr.dbservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

@Configuration
public class DynamoDBConfig {

	@Value("${cloud.aws.dynamodb.endpoint}")
	private String amazonDynamoDBEndpoint;

	@Value("${cloud.aws.credentials.accessKey}")
	private String amazonAWSAccessKey;

	@Value("${cloud.aws.credentials.secretKey}")
	private String amazonAWSSecretKey;

	@Value("${cloud.aws.dynamodb.region}")
	private String amazonAWSRegion;

	@Bean
	public AWSCredentials amazonAWSCredentials() {
		return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
	}

	@Bean
	public AmazonDynamoDBClient amazonDynamoDBClient(AWSCredentials awsCredentials) {
		AmazonDynamoDBClient amazonDynamoDbClient = new AmazonDynamoDBClient(awsCredentials);

		amazonDynamoDbClient.setEndpoint(amazonDynamoDBEndpoint);
		amazonDynamoDbClient.setRegion(Region.getRegion(Regions.fromName(amazonAWSRegion)));
		return amazonDynamoDbClient;

		// return
		// AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
		// new
		// EndpointConfiguration(amazonDynamoDBEndpoint,amazonAWSRegion)).build();

		/*
		 * AmazonDynamoDB amazonDynamoDB = new
		 * AmazonDynamoDBClient(amazonAWSCredentials()); if
		 * (!StringUtils.isEmpty(amazonDynamoDBEndpoint)) {
		 * amazonDynamoDB.setEndpoint(amazonDynamoDBEndpoint); }
		 */
	}
	
}