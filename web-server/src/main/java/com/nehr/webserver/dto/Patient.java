package com.nehr.webserver.dto;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Patient {

	
	String nID;

	String firstName;

	String email;

	String lastName;

	int dateDay;

	int dateMonth;

	int dateYear;

	String gender;

	String mobileNumber;

	String passwordHash;

	ArrayList<String> doctorEmail;
	
	String birthday;
	

	public Patient(@JsonProperty("nID") String nID, @JsonProperty("firstName") String firstName,
			@JsonProperty("lastName") String lastName, @JsonProperty("dateDay") int dateDay,
			@JsonProperty("dateMonth") int dateMonth, @JsonProperty("dateYear") int dateYear,
			@JsonProperty("gender") String gender, @JsonProperty("mobileNumber") String mobileNumber,
			@JsonProperty("passwordHash") String passwordHash, @JsonProperty("email") String email, @JsonProperty("doctorEmail") ArrayList<String> doctorEmail) {
		super();
		this.nID = nID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateDay=dateDay;
		this.dateMonth=dateMonth;
		this.dateYear=dateYear;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.passwordHash = passwordHash;
		this.email = email;
		this.doctorEmail = doctorEmail;
	}


	public String getnID() {
		return nID;
	}


	public void setnID(String nID) {
		this.nID = nID;
	}

	
	public int getDateDay() {
		return dateDay;
	}


	public void setDateDay(int dateDay) {
		this.dateDay = dateDay;
	}


	public int getDateMonth() {
		return dateMonth;
	}


	public void setDateMonth(int dateMonth) {
		this.dateMonth = dateMonth;
	}


	public int getDateYear() {
		return dateYear;
	}


	public void setDateYear(int dateYear) {
		this.dateYear = dateYear;
	}


	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public ArrayList<String> getDoctorEmail() {
		return doctorEmail;
	}


	public void setDoctorEmail(ArrayList<String> doctorEmail) {
		this.doctorEmail = doctorEmail;
	}

	public String getBirthday() {
		return birthday;
	}


	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
}
