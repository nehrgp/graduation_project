package com.nehr.analytics.models;

import java.util.Map;

public class AnalyticsModel {
	Map<String, Integer> data;

	public Map<String, Integer> getData() {
		return data;
	}

	public void setData(Map<String, Integer> data) {
		this.data = data;
	}
}
