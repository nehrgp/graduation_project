package com.nehr.analytics.models;

import java.util.HashMap;

public class PatientSmokersByGender {

	HashMap<String, Integer> SmokersPercentages = new HashMap<String, Integer>();

	public HashMap<String, Integer> getSmokersPercentages() {
		return SmokersPercentages;
	}

	public void setSmokersPercentages(HashMap<String, Integer> smokersPercentages) {
		SmokersPercentages = smokersPercentages;
	}
	
}


