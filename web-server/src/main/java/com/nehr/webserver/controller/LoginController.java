package com.nehr.webserver.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nehr.webserver.clients.DbserviceClient;
import com.nehr.webserver.dbmodels.DoctorModel;
import com.nehr.webserver.dto.BusinessClient;
import com.nehr.webserver.dto.LoginModel;
import com.nehr.webserver.dto.Patient;


@Controller
public class LoginController {
	
	@Autowired
	DbserviceClient dbserviceClient;

	
    @RequestMapping(value = "/user/login", method = RequestMethod.GET)
	public String navToLogin() {
 	    return "login"; 
	}

    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public String login(LoginModel loginModel,HttpSession session) {
    	
    	Patient patient = dbserviceClient.getPatientByMobile(loginModel.getMobileNumber()) ;
    	
		if (patient != null) {
		
			if (loginModel.getPassword().equals(patient.getPasswordHash()) ) {
				session.setAttribute("loggedInUserId", patient.getnID());
				session.setAttribute("mobileNumber", patient.getMobileNumber());
				session.setAttribute("loggedInUserType", "PATIENT");
				return "redirect:/user/update/personal-info";
			} else {
				return "passwords-don't-match";
			}
		}

		else
			return "signup";

	}    
}
