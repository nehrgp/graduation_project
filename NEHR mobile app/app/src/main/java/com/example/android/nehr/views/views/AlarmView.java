package com.example.android.nehr.views.views;

import com.example.android.nehr.models.DrugHistory;

import java.util.List;

public interface AlarmView extends BaseView {
    void showDrugs(List<DrugHistory> drugHistory);
    void fail();
}
