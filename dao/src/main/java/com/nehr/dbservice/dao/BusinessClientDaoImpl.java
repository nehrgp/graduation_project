package com.nehr.dbservice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.nehr.dbservice.models.BusinessClient;

@Repository
public class BusinessClientDaoImpl implements BusinessClientDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;

	@Override
	public BusinessClient upsertBusinessClient(BusinessClient businessClient) {

		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		try {
			mapper.save(businessClient, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return findBusinessClientByEmail(businessClient.getEmail());
		}
	}

	@Override
	public void deleteBusinessClientByuserName(String email) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		BusinessClient businessClient = new BusinessClient();
		businessClient.setEmail(email);
		mapper.delete(businessClient);
	}

	@Override
	public List<BusinessClient> findAll() {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		return mapper.scan(BusinessClient.class, new DynamoDBScanExpression());
	}

	@Override
	public BusinessClient findBusinessClientByEmail(String email) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		BusinessClient businessClient = new BusinessClient();
		businessClient.setEmail(email);
		BusinessClient result = null;
		try {
			result = mapper.load(businessClient);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;
		}
	}
}