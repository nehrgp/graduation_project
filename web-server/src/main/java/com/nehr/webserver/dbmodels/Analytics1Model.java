package com.nehr.webserver.dbmodels;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Analytics1Model {
	String col1_name;
	String col2_name;
	Map<String, Integer> data;
	
	public Analytics1Model(){};
	
	public Analytics1Model(@JsonProperty("col1_name")String col1_name, @JsonProperty("col2_name")String col2_name, @JsonProperty("data")Map<String, Integer> data) {
		super();
		this.col1_name = col1_name;
		this.col2_name = col2_name;
		this.data = data;
	}
	
	public String getCol1_name() {
		return col1_name;
	}
	public void setCol1_name(String col1_name) {
		this.col1_name = col1_name;
	}
	
	public String getCol2_name() {
		return col2_name;
	}
	public void setCol2_name(String col2_name) {
		this.col2_name = col2_name;
	}
	
	public Map<String, Integer> getData() {
		return data;
	}
	public void setData(Map<String, Integer> data) {
		this.data = data;
	}
	
}
