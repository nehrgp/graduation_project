package com.nehr.dbservice.models;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "patient")
public class PatientDrugHistory {

	private String nID;
	private List<DrugHistory> drugs;
	
	
	
	
	@DynamoDBHashKey(attributeName = "nID")
	public String getnId() {
		return nID;
	}

	public void setnId(String nID) {
		this.nID = nID;
	}

	@DynamoDBAttribute(attributeName = "drugs")
	public List<DrugHistory> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<DrugHistory> drugs) {
		this.drugs = drugs;
	}
	
	
}

