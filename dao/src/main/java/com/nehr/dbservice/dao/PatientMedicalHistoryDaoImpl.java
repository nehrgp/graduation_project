package com.nehr.dbservice.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.nehr.dbservice.models.Drug;
import com.nehr.dbservice.models.PatientMedicalHistory;


@Repository
public class PatientMedicalHistoryDaoImpl implements PatientMedicalHistoryDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;
	
	@Override
	public PatientMedicalHistory upsertPatientMedicalHistory (PatientMedicalHistory patientMedicalHistory) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		try {
			mapper.save(patientMedicalHistory, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return patientMedicalHistory;
		}
	}

	@Override
	public PatientMedicalHistory findPatientMedicalHistoryByNID(String nID) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		PatientMedicalHistory patientMedicalHistory = new PatientMedicalHistory();
		patientMedicalHistory.setnID(nID);
		PatientMedicalHistory result = null;
		try {
			result = mapper.load(patientMedicalHistory);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;

		}
	}
		
}
