package com.nehr.dbservice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.nehr.dbservice.models.MobileInstance;


@Repository
public class MobileInstanceDaoImpl implements MobileInstanceDao {
	
	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;

	@Override
	public void setMobileInstanceId(String nID, String InstanceIdToken) {
		MobileInstance mobileInstance=new MobileInstance();
		mobileInstance.setnId(nID);;
		mobileInstance.setMobileInstanceIdToken(InstanceIdToken);
		
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		try {
			mapper.save(mobileInstance, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}

	@Override
	public MobileInstance getMobileInstance(String nId) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		MobileInstance user = new MobileInstance();
		user.setnId(nId);
		MobileInstance result = null;
		try {
			result = mapper.load(user);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;

		}

	}

}
