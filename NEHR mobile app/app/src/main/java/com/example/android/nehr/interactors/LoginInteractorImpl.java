package com.example.android.nehr.interactors;

import android.util.Log;

import com.example.android.nehr.data.DBClientImpl;
import com.example.android.nehr.data.DBClient;
import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.presenters.LoginPresenter;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by omar on 1/30/2018.
 */

public class LoginInteractorImpl implements LoginInteractor {

    private DBClient dbClient;
    Observer<LoginModel> observer;

    public LoginInteractorImpl() {
        this.dbClient = new DBClientImpl();
    }

    @Override
        public void callLoginAPI(LoginPresenter loginPresenter, LoginModel loginModel) {

        observer=new Observer<LoginModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(LoginModel returnedLoginModel) {
                if (returnedLoginModel != null && returnedLoginModel.getPasswordHash().equals(loginModel.getPasswordHash())) {
                    loginPresenter.loginSuccess(returnedLoginModel);

                } else {
                    loginPresenter.loginFailure();

                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d("ourErrorMsg", e.getMessage());

                Log.d("ourMsg", "maybeNetworkError");



            }

            @Override
            public void onComplete() {

            }
        };
        dbClient.getBasicInfoByMobileNumber(loginModel.getMobileNumber())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }


}
