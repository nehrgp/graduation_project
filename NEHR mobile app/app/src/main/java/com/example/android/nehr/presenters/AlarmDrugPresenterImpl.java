package com.example.android.nehr.presenters;

import com.example.android.nehr.interactors.DrugInteractor;
import com.example.android.nehr.interactors.DrugInteractorImpl;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.AlarmView;

import java.util.List;

public class AlarmDrugPresenterImpl implements DrugPresenter {

    DrugInteractor interactor;
    AlarmView view;

    public AlarmDrugPresenterImpl(AlarmView view) {
        interactor = new DrugInteractorImpl();
        this.view=view;
    }

    @Override
    public void getMyDrugs() {
        interactor.callGetDrugAPI( this,SaveSharedPreference.getnId(view.getContext()));
    }

    @Override
    public void success(List<DrugHistory> drugHistory) {
        view.showDrugs(drugHistory);

    }

    @Override
    public void fail() {

        view.fail();

    }
}
