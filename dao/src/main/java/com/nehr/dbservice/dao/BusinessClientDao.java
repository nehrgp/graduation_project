package com.nehr.dbservice.dao;

import java.util.List;

import com.nehr.dbservice.models.BusinessClient;


public interface BusinessClientDao {

	BusinessClient upsertBusinessClient (BusinessClient businessClient);
	BusinessClient findBusinessClientByEmail(String email);
	void deleteBusinessClientByuserName(String email);
	List<BusinessClient> findAll();
}
