package com.nehr.webserver.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.nehr.webserver.clients.DbserviceClient;
import com.nehr.webserver.dbmodels.DrugHistory;
import com.nehr.webserver.dbmodels.PatientDrugHistory;
import com.nehr.webserver.dto.Drug;
import com.nehr.webserver.dto.MyDrug;


@Controller
public class MyDrugController {
	
	@Autowired
	DbserviceClient dbserviceClient;

	
	@RequestMapping(value = "/user/profile/mydrugs", method = RequestMethod.GET)
	public ModelAndView getMyDrugsView(HttpSession session) {
		
		if (session.getAttribute("loggedInUserId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			List<DrugHistory> drugs = dbserviceClient.getPatientDrugList((String)session.getAttribute("loggedInUserId"));
			ModelAndView model = new ModelAndView("myDrug");
			model.addObject("drugs", drugs);
			return model; 
		}
	}

	@RequestMapping(value = "/user/profile/mydrugs/{nID}", method = RequestMethod.GET)
	public ModelAndView getMyDrugsViewByID(@PathVariable("nID") String nID) {
		List<DrugHistory> drugs = dbserviceClient.getPatientDrugList(nID);
		ModelAndView model = new ModelAndView("businessClients/doctor/patientDrug");
		model.addObject("drugs", drugs);
		return model; 
	}
	
	 @RequestMapping(value = "/user/profile/mydrugs", method = RequestMethod.POST)
		public ModelAndView AddaDrug(MyDrug drug,HttpSession session ) {
		 PatientDrugHistory patientDrugs= new PatientDrugHistory();
		 DrugHistory drugHistory = new DrugHistory();
		 drugHistory.setName(drug.getName());
		 
		 //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		 //Date parsed = format.parse(drug.getStartDate());
		 
		 //drugHistory.setStartDate(drug.getStartDateDay()+"/"+drug.getStartDateMonth()+"/"+drug.getStartDateYear());
		 drugHistory.setStartDate(drug.getStartDate());
		 
		 //drugHistory.setEndDate(drug.getEndDateDay()+"/"+drug.getEndDateMonth()+"/"+drug.getEndDateYear());
		 drugHistory.setEndDate(drug.getEndDate());
		 
		 Drug drugDynamo = dbserviceClient.findDrugByName(drug.getName());
		 if(drugDynamo != null) {
			 drugHistory.setImage(drugDynamo.getImage()); 
		 }
		 
		 List<DrugHistory> drugs = new ArrayList<>();
		 drugs.add(drugHistory);
		 
		 patientDrugs.setDrugs(drugs);
		 
		 String nID;
		 if((String)session.getAttribute("loggedInUserId") != null) {
			 nID = (String)session.getAttribute("loggedInUserId");
		 } else {
			 nID = (String)session.getAttribute("patientnID");
		 }
		 
		 patientDrugs.setnId(nID);
		 
		 dbserviceClient.addDrugToPatientDrugList(patientDrugs);
		 
	    	return new ModelAndView("redirect:/user/profile/mydrugs");
	    	
		}
}
