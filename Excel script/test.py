import requests
import xlrd
import xlutils.copy

fileName = "Trial.xls"
workbook = xlrd.open_workbook(fileName)
outBook = xlutils.copy.copy(workbook)
WorkSheet = workbook.sheet_by_index(0)

RowNumber =  WorkSheet.nrows
ColNumber = WorkSheet.ncols

for RowCounter in range(0,RowNumber): #Loop through all rows in worksheet
	name = str(WorkSheet.cell(RowCounter,0).value)
	description = str(WorkSheet.cell(RowCounter,1).value)
	diseases = []
	diseases.append(str(WorkSheet.cell(RowCounter,2).value))
	effectiveSubstance = []
	effectiveSubstance.append(str(WorkSheet.cell(RowCounter,3).value))
	symptoms = []
	symptoms.append(str(WorkSheet.cell(RowCounter,4).value))
	active = bool(str(WorkSheet.cell(RowCounter,5).value))
	payload = {"name": name,"description":description,"diseases":diseases,"effectiveSubstance":effectiveSubstance,"symptoms":symptoms,"active":active}
	r = requests.post('http://18.191.180.56:8090/drug/upsert',json = payload )
	print r.status_code, payload
'''
	http://18.191.180.56:8090
	http://18.217.96.80:8090
'''