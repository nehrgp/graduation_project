package com.nehr.dbservice.models;

public class AddDrug {
	String name;
	int startDateDay;
	int startDateMonth;
	int startDateYear;
	int endDateDay;
	int endDateMonth;
	int endDateYear;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStartDateDay() {
		return startDateDay;
	}
	public void setStartDateDay(int startDateDay) {
		this.startDateDay = startDateDay;
	}
	public int getStartDateMonth() {
		return startDateMonth;
	}
	public void setStartDateMonth(int startDateMonth) {
		this.startDateMonth = startDateMonth;
	}
	public int getStartDateYear() {
		return startDateYear;
	}
	public void setStartDateYear(int startDateYear) {
		this.startDateYear = startDateYear;
	}
	public int getEndDateDay() {
		return endDateDay;
	}
	public void setEndDateDay(int endDateDay) {
		this.endDateDay = endDateDay;
	}
	public int getEndDateMonth() {
		return endDateMonth;
	}
	public void setEndDateMonth(int endDateMonth) {
		this.endDateMonth = endDateMonth;
	}
	public int getEndDateYear() {
		return endDateYear;
	}
	public void setEndDateYear(int endDateYear) {
		this.endDateYear = endDateYear;
	}
	
	
	
	
	

}
