package com.nehr.notificationservice.email;



import com.nehr.notificationservice.models.EmailModel;

public interface EmailService {
	
    void sendSimpleMessage(EmailModel emailModel);
    void sendHtmlMessageUsingTemplate(EmailModel emailModel);
}