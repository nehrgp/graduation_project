package com.nehr.dbservice.models;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "prescription")
public class Prescription {

	private String doctor_name;
	private String notes;
	private String patient_nId;
	private String url;
	private String date;
	
	@DynamoDBAttribute(attributeName = "doctor_name")
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	
	@DynamoDBAttribute(attributeName = "notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@DynamoDBIndexHashKey(globalSecondaryIndexName="nId-index" , attributeName = "patient_nId")
	public String getPatient_nId() {
		return patient_nId;
	}
	public void setPatient_nId(String patient_nId) {
		this.patient_nId = patient_nId;
	}
	
	@DynamoDBHashKey(attributeName = "url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@DynamoDBAttribute(attributeName = "date")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

}
