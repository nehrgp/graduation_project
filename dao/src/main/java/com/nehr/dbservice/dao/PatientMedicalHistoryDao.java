package com.nehr.dbservice.dao;

import com.nehr.dbservice.models.PatientMedicalHistory;

public interface PatientMedicalHistoryDao {

	PatientMedicalHistory upsertPatientMedicalHistory (PatientMedicalHistory patientMedicalHistory);
	PatientMedicalHistory findPatientMedicalHistoryByNID(String nID);

}
