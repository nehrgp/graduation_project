package com.nehr.webserver.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class IndexController {
	@RequestMapping(value = "/", method = RequestMethod.GET) 
	public String navToLogin(HttpSession session) { 
		//session.removeAttribute("loggedInUserId");
		//TODO:remove this after making a logout button
	    return "index"; 
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.removeAttribute("loggedInUserId");
		session.removeAttribute("loggedInDoctorName");
		session.removeAttribute("loggedInDoctorEmail");
		session.removeAttribute("patientnID");
		session.removeAttribute("loggedInUserType");
		return "redirect:/";
	}
}
