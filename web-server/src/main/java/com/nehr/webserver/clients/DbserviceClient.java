package com.nehr.webserver.clients;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nehr.webserver.Config;
import com.nehr.webserver.dbmodels.DrugHistory;
import com.nehr.webserver.dbmodels.PatientDrugHistory;
import com.nehr.webserver.dbmodels.PushNotification;
import com.nehr.webserver.dto.AuthenticDoctor;
import com.nehr.webserver.dto.BusinessClient;
import com.nehr.webserver.dto.Drug;
import com.nehr.webserver.dto.Patient;

@Component
public class DbserviceClient {
@Autowired
Config config ;


	public Patient getPatientByMobile(String mobileNumber){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Patient> response =restTemplate.getForEntity(config.getDbserviceUrl() + "patient/find/mobileNumber/" + mobileNumber,Patient.class);
		if (response != null && response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
			Patient patient=response.getBody();
			return patient ;
		}
	   return null ;
	}
	
	public Patient getPatientByNId(String id){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Patient> response =restTemplate.getForEntity(config.getDbserviceUrl() + "patient/find/nid/" + id,Patient.class);
		if (response != null && response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
			Patient patient=response.getBody();
			return patient ;
		}
	   return null ;
	}
	
	
	public  void  updatePatientPersonalInfo(Patient patient){
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Patient> request = new HttpEntity<>(patient);
		restTemplate.postForObject(config.getDbserviceUrl() + "patient/upsert", request, Patient.class);
	}
	
	public List<Drug> findAllDrugs(){
	
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Drug[]> responseEntity = restTemplate.getForEntity(config.getDbserviceUrl()+"drug/find/all", Drug[].class);
		List<Drug> drugs = Arrays.asList(responseEntity.getBody());
		 return drugs ;
	}  
	
	public  void  addDrugToPatientDrugList(PatientDrugHistory patientDrugHistory){
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<PatientDrugHistory> request = new HttpEntity<>(patientDrugHistory);
		restTemplate.postForObject(config.getDbserviceUrl() + "pdh/insert", request, Patient.class);
	}
	
	public List<DrugHistory> getPatientDrugList(String nId){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<PatientDrugHistory> response = restTemplate.getForEntity(config.getDbserviceUrl()+"/pdh/find/"+nId, PatientDrugHistory.class);
		PatientDrugHistory patientHistory=response.getBody();
		 return patientHistory.getDrugs();
	}
	
	public BusinessClient getBusinessClientByemail(String email) {
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<BusinessClient> response = restTemplate.getForEntity(config.getDbserviceUrl() + "/businessclient/find/email/" + email + "/" ,BusinessClient.class);
		if (response != null && response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
			BusinessClient businessClient = response.getBody();
			return businessClient ;
		}
	   return null ;
		
	}

	public void upsertBusinessClient(BusinessClient client) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<BusinessClient> request = new HttpEntity<>(client);
		restTemplate.postForObject(config.getDbserviceUrl() + "/businessclient/upsert/", request, BusinessClient.class);
	}
	
	public List<Drug> getDrugsByPrefix(String prefix) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Drug[]> response = restTemplate.getForEntity(config.getDbserviceUrl()+"drug/find/prefix/"+prefix, Drug[].class);
		List<Drug> drugs = Arrays.asList(response.getBody());
		return drugs;
	}

	public Boolean isAuthenticated(String nID, String email) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Boolean> response = restTemplate.getForEntity(config.getDbserviceUrl()+"/authenticDoctor/find/nID/" + email + "/" + nID , Boolean.class);
		Boolean result = response.getBody();
		return result;
	}
	
	public void addAuthenticatedDoctorEmail(AuthenticDoctor authenticDoctor) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<AuthenticDoctor> request = new HttpEntity<>(authenticDoctor);
		restTemplate.postForObject(config.getDbserviceUrl() + "/authenticDoctor/add/nID", request, AuthenticDoctor.class);
	}

	public AuthenticDoctor findPatiendNIDByAuthenticDoctorEmail(String doctorEmail) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AuthenticDoctor> response = restTemplate.getForEntity(config.getDbserviceUrl() + "/authenticDoctor/find/nID/" + doctorEmail + "/" ,AuthenticDoctor.class);
		if (response != null && response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
			AuthenticDoctor authenticDoctor = response.getBody();
			return authenticDoctor ;
		}
	   return null ;
	}

	public Drug findDrugByName(String drugName) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Drug> response =restTemplate.getForEntity(config.getDbserviceUrl() + "/drug/find/name/" + drugName,Drug.class);
		if (response != null && response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
			Drug drug =response.getBody();
			return drug;
		}
	   return null ;
	}
	
	public String getMobileTokenByID(String nID){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(config.getDbserviceUrl() + "mobileInstance/token/get/nId/" + nID,String.class);
		if (response != null && response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
			String token = response.getBody();
			return token;
		}
	   return null ;
	}
	
	public void pushNotification(PushNotification push) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<PushNotification> request = new HttpEntity<>(push);
		restTemplate.postForObject("http://18.188.137.233:8015/push/", request, String.class);
	}
}
