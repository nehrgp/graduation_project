package com.nehr.webserver.dbmodels;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PushNotification {

	String to;
	Map <String, String> data;
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	public PushNotification() {
		// TODO Auto-generated constructor stub
	}

	public PushNotification(@JsonProperty("to")String to, @JsonProperty("data") Map<String, String> data) {
		super();
		this.to = to;
		this.data = data;
	}

}
