package com.nehr.dbservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.PatientDrugHistoryDao;
import com.nehr.dbservice.models.PatientDrugHistory;
@RestController
public class PatientDrugHistoryController {
	
	
	@Autowired
	PatientDrugHistoryDao patientDrugHistoryDao ;
	
	@RequestMapping(value="/pdh/insert",method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PatientDrugHistory insertDrug(@RequestBody PatientDrugHistory patientDrugHistory){
    	 return patientDrugHistoryDao.insertDrug(patientDrugHistory);
    }
    
    @RequestMapping(value="/pdh/delete/{patientId}/{drugName}",method=RequestMethod.DELETE)
    public void deleteDrugById(@PathVariable("patientId") String patientId,@PathVariable("drugName") String drugName){
    	patientDrugHistoryDao.deleteDrugByName(patientId,drugName);
    	 		 
    }
    
  
    
    @RequestMapping(value="/pdh/find/{patientId}",method=RequestMethod.GET)
    public PatientDrugHistory findAllDrugs(@PathVariable("patientId") String patientId){
    	return patientDrugHistoryDao.findPatientDrugHistory(patientId);
    }
    
    @RequestMapping(value="/pdh/contradict/{drug1}/{drug2}",method=RequestMethod.GET)
    public Boolean doesContradict(@PathVariable("drug1") String drug1,@PathVariable("drug2") String drug2){
    	 return patientDrugHistoryDao.contradict(drug1, drug2);
    }
    
	
	
}
