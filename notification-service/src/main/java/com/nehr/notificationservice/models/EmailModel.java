package com.nehr.notificationservice.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class EmailModel {

	private String recipient;
	private String body;
	private String subject;

	@JsonCreator
	public EmailModel(@JsonProperty("recipient") String recipient, @JsonProperty("body") String body,
			@JsonProperty("subject") String subject, @JsonProperty("type") String type) {
		this.recipient = recipient;
		this.body = body;
		this.subject = subject;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	
}
