package com.example.android.nehr.presenters;

import com.example.android.nehr.interactors.DrugInteractor;
import com.example.android.nehr.interactors.DrugInteractorImpl;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.MyDrugsDetailsView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omar on 2/15/2018.
 */

public class MyDrugHistoryDetailsPresenterImpl implements MyDrugHistoryDetailsPresenter {

    MyDrugsDetailsView view ;
    DrugInteractor interactor;

    public MyDrugHistoryDetailsPresenterImpl(MyDrugsDetailsView view) {
        this.view = view;
        this.interactor = new DrugInteractorImpl();
    }

    @Override
    public void addToMyDrugs(DrugHistory drugHistory) {
        PatientDrugHistory patientDrugHistory= this.fromDrugHistoryToPatientDrugHistory(drugHistory);
        interactor.callAddDrugHistoryAPI(this,patientDrugHistory);
    }

    @Override
    public void drugAddedSuccesfully() {
      view.drugShowSuccess();
    }

    @Override
    public void drugAddingFailed() {
      view.drugShowFailed();
    }

    private PatientDrugHistory fromDrugHistoryToPatientDrugHistory(DrugHistory drugHistory){
        PatientDrugHistory patientDrugHistory=new PatientDrugHistory();
        patientDrugHistory.setnId(SaveSharedPreference.getnId(view.getContext()));
        List<DrugHistory> drugHistoryList=new ArrayList<DrugHistory>();
        drugHistoryList.add(drugHistory);
        patientDrugHistory.setDrugs(drugHistoryList);
        return patientDrugHistory;

    }

}
