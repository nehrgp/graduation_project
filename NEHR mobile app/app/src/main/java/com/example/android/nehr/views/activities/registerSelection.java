package com.example.android.nehr.views.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.View;

import com.example.android.nehr.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class registerSelection extends AppCompatActivity {

    @BindView(R.id.simpleRegisterationCard) CardView simpleRegister ;
    @BindView(R.id.totalRegistertaionCard) CardView totalRegister ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_selection);
        ButterKnife.bind(this);

        simpleRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),registerationUsingMinimalData.class);
                startActivity(intent);
                finish();
            }
        });
        totalRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),signupActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            this.startActivity(new Intent(registerSelection.this,loginActivity.class));
        }
        return true;
    }
}
