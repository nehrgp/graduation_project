package com.nehr.webserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.webserver.dbmodels.Analytics1Model;

@Controller
@RequestMapping("/analytics")
public class AnalyticsController {

	@GetMapping(value = "")
	public String redirect() {
		return "redirect:/analytics/";
	}
	
	@GetMapping(value = "/")
	public String pieChart() {
		return "analytics/analytics1";
	}
	
	@GetMapping(value = "/object")
	@ResponseBody
	public Analytics1Model createObject() {
		Analytics1Model model = new Analytics1Model();
		Map<String, Integer> data = new HashMap();
		
		data.put("Age 1", 5);
		data.put("Age 2", 10);
		data.put("Age 3", 5);
		data.put("Age 4", 15);
		
		model.setCol1_name("asd");
		model.setCol2_name("num");
		model.setData(data);
		return model;
	}
}
