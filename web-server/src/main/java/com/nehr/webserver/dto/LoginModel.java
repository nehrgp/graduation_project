package com.nehr.webserver.dto;

import org.hibernate.validator.constraints.NotEmpty;



public class LoginModel {

	@NotEmpty
	String mobileNumber;
	@NotEmpty
	String password;

	public LoginModel(){};
	public LoginModel( String mobileNumber,String password) {
		super();
		this.mobileNumber = mobileNumber;
		this.password = password;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}
