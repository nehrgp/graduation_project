package com.nehr.dbservice.dao;
 

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.nehr.dbservice.models.Prescription;


@Repository
public class PrescriptionDaoImpl implements PrescriptionDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;
	
	
	@Override
	public Prescription addPrescription(Prescription prescription) {
		
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		try {
			mapper.save(prescription, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return this.findPrescriptionByUrl(prescription.getUrl());
		}
	}
	
	@Override
	public Prescription findPrescriptionByUrl(String url) {	
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		Prescription prescription = new Prescription();
		prescription.setUrl(url);
		Prescription result = null;
		try {
			result = mapper.load(prescription);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	@Override
	public List<Prescription> findPrescriptionById(String patient_nId) {
		return this.getPrescriptionsUsingContainFilter("patient_nId", patient_nId);
	}
	
	private List<Prescription> getPrescriptionsUsingContainFilter(String attributeName, String attributeValue) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		HashMap<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(attributeValue));
		HashMap<String, String> expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeNames.put("#att1", attributeName);

		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
				.withFilterExpression("contains(#att1, :val1)").withExpressionAttributeValues(eav)
				.withExpressionAttributeNames(expressionAttributeNames);
		List<Prescription> Prescriptions = mapper.scan(Prescription.class, scanExpression);

		return Prescriptions;
	}

}
