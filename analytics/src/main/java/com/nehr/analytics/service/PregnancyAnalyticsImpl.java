package com.nehr.analytics.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class PregnancyAnalyticsImpl implements PregnancyAnalytics {

	static final String DB_URL = "jdbc:redshift://data-warehouse.copyte5s6ypv.us-east-2.redshift.amazonaws.com:5439/dev";
	static final String USER = "hossam26644";
	static final String PASS = "4176532Ab";

	@Override
	public Map<String, Integer> getPregnantsByAge() {
		String baseQuery = " select count(nid) from patientanalytics WHERE pregnant = '1' and dateyear >=";
		int sum = 0;

		String FirstSector, SecondSector, ThirdSector, FourthSector, FifthSector, sixthSector;
		int year = Calendar.getInstance().get(Calendar.YEAR);

		String query = baseQuery + "'" + String.valueOf(year - 10) + "'";
		FirstSector = this.performQuery(query);
		int FirstSegment = Integer.parseInt(FirstSector);
		sum += FirstSegment;

		query = baseQuery + "'" + String.valueOf(year - 20) + "'";
		SecondSector = this.performQuery(query);
		int SecondSegment = Integer.parseInt(SecondSector) - sum;
		sum += SecondSegment;

		query = baseQuery + "'" + String.valueOf(year - 30) + "'";
		ThirdSector = this.performQuery(query);
		int ThirdSegment = Integer.parseInt(ThirdSector) - sum;
		sum += ThirdSegment;

		query = baseQuery + "'" + String.valueOf(year - 40) + "'";
		FourthSector = this.performQuery(query);
		int FourthSegment = Integer.parseInt(FourthSector) - sum;
		sum += FourthSegment;

		query = baseQuery + "'" + String.valueOf(year - 50) + "'";
		FifthSector = this.performQuery(query);
		int FifthSegment = Integer.parseInt(FifthSector) - sum;
		sum += FifthSegment;

		query = baseQuery + "'" + String.valueOf(year - 60) + "'";
		sixthSector = this.performQuery(query);
		int sixthSegment = Integer.parseInt(sixthSector) - sum;

		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("10", FirstSegment);
		slides.put("20", SecondSegment);
		slides.put("30", ThirdSegment);
		slides.put("40", FourthSegment);
		slides.put("50", FifthSegment);
		slides.put("120", sixthSegment);

		return slides;
	}

	@Override
	public Map<String, Integer> getAlcholicPrgnants() {
		String query = " select count(nid) from patientanalytics WHERE pregnant = '1' and alcoholic = '1'";
		Integer alcholicPregnant = Integer.parseInt(this.performQuery(query));
		query = " select count(nid) from patientanalytics WHERE pregnant = '1' and alcoholic = '0'";
		Integer nonAlcoholicPregnant = Integer.parseInt(this.performQuery(query));

		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("alcoholicPregnant", alcholicPregnant);
		slides.put("nonAlcoholicPregnant", nonAlcoholicPregnant);
		return slides;
	}

	@Override
	public Map<String, Integer> getSmokerPregnants() {
		String query = " select count(nid) from patientanalytics WHERE pregnant = '1' and smoker = '1'";
		Integer smokerPregnant = Integer.parseInt(this.performQuery(query));
		query = " select count(nid) from patientanalytics WHERE pregnant = '1' and smoker = '0'";
		Integer nonSmokerPregnant = Integer.parseInt(this.performQuery(query));

		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("smokerPregnant", smokerPregnant);
		slides.put("nonSmokerPregnant", nonSmokerPregnant);
		return slides;

	}

	@Override
	public Map<String, Integer> getCleanPregnants() {
		String query = " select count(nid) from patientanalytics WHERE pregnant = '1' and smoker = '0 and alcoholic = '0'";
		Integer cleanPregnant = Integer.parseInt(this.performQuery(query));

		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("cleanPregnant", cleanPregnant);
		return slides;

	}

	@Override
	public Map<String, Integer> getAlcholicSmokerPregnats() {
		String query = " select count(nid) from patientanalytics WHERE pregnant = '1' and smoker = '1 and alcoholic = '1'";
		Integer alcholicSmokerPregnats = Integer.parseInt(this.performQuery(query));

		query = " select count(nid) from patientanalytics WHERE pregnant = '1' ";
		Integer allPregnants = Integer.parseInt(this.performQuery(query));

		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("alcholicSmokerPregnats", alcholicSmokerPregnats);
		slides.put("cleanPregnants", allPregnants-alcholicSmokerPregnats);
		return slides;
	}

	private String performQuery(String query) {

		Connection conn = null;
		Statement stmt = null;

		// Query = "select count(nid) from patientanalytics WHERE dateyear >=" +
		// PastYears;

		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.amazon.redshift.jdbc.Driver");

			// STEP 3: Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				// Retrieve two columns.
				String result = rs.getString("count");

				// Display values.
				return result;
			}

			stmt.close();
			conn.close();
			rs.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		return "ConnectionError";

	}

}
