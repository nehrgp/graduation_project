package com.example.android.nehr.data;

import android.util.Log;

import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.models.MobileInstance;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.models.SignupModel;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mahmoud on 2/8/2018.
 */

public class RemoteDBClient {

    public SignupModel upsertBasicInfo(SignupModel signupModel){
        final String url = DataConfig.getDbServiceUrl()+"/patient/upsert";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<SignupModel> request = new HttpEntity(signupModel);
        return (SignupModel) restTemplate.postForObject(url, request, SignupModel.class);
    }

    public LoginModel getBasicInfoByMobileNumber(String mobileNumber) {
        final String url = DataConfig.getDbServiceUrl()+"/patient/find/mobileNumber/" + mobileNumber;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return (LoginModel) restTemplate.getForObject(url, LoginModel.class);
    }

    public List<DrugModel> getDrugsByPrefix(String prefix){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<DrugModel[]> responseEntity = restTemplate.getForEntity(DataConfig.getDbServiceUrl()+"/drug/find/prefix/"+prefix, DrugModel[].class);
        List<DrugModel> drugs = Arrays.asList(responseEntity.getBody());
        return drugs ;
    }

    public  PatientDrugHistory  addToPatientDrugHistory(PatientDrugHistory patientDrugHistory){
        final String url = DataConfig.getDbServiceUrl()+"/pdh/insert";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<PatientDrugHistory> request = new HttpEntity<>(patientDrugHistory);
        return (PatientDrugHistory) restTemplate.postForObject(url, request, PatientDrugHistory.class);
    }

    public List<DrugHistory> getPatientDrugs(String id){
        String url=DataConfig.getDbServiceUrl()+"/pdh/find/"+id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return ((PatientDrugHistory) restTemplate.getForObject(url, PatientDrugHistory.class)).getDrugs();

    }

    public MobileInstance saveToken(MobileInstance mobileInstance){
        String url=DataConfig.getDbServiceUrl()+"/mobileInstance/token/set";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<MobileInstance> request = new HttpEntity<>(mobileInstance);
        return (MobileInstance)restTemplate.postForObject(url, request, MobileInstance.class);

    }

    public String sendAuthorizationResponse(String response, String doctorEmail, String patientNID){
        String url=DataConfig.webServerUrl+"/doctor/request?value="+response+"&doctor="+doctorEmail+"&patient="+patientNID;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getForEntity(url,String.class);
        return "success";
    }
}
