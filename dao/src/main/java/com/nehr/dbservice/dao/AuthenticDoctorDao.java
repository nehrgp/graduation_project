package com.nehr.dbservice.dao;

import com.nehr.dbservice.models.AuthenticDoctor;

public interface AuthenticDoctorDao {

	AuthenticDoctor findPatientID(String doctorEmail);
	AuthenticDoctor addPatientID(AuthenticDoctor authenticDoctor);
	Boolean isAuthentic(String patient_id, String docotrEmail);
}
