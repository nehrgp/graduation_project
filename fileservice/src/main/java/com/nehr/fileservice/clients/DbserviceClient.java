package com.nehr.fileservice.clients;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nehr.fileservice.Config;
import com.nehr.fileservice.dto.PrescriptionDao;


@Component
public class DbserviceClient {
@Autowired
Config config ;

	public void addPrescription(PrescriptionDao prescriptionDao){
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<PrescriptionDao> request = new HttpEntity<>(prescriptionDao);
		restTemplate.postForObject(config.getDbserviceUrl() + "prescription/addPrescription", request, PrescriptionDao.class);
	}
}
