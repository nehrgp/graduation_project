package com.nehr.webserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MailConfig {

	@Value("${nehr.mailservice.url}")
	private String mailserviceUrl;

	public String getMailserviceUrl() {
		return mailserviceUrl;
	}

	public void setMailserviceUrl(String mailserviceUrl) {
		this.mailserviceUrl = mailserviceUrl;
	}
}
