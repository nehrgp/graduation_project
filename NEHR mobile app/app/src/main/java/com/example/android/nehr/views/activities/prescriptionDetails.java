package com.example.android.nehr.views.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.models.PrescriptionModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class prescriptionDetails extends NavigationDrawer {


    @BindView(R.id.doctorNameTxt)
    TextView doctorNameTxt ;
    @BindView(R.id.presDateTxt)
    TextView prescriptionDateTxt ;
    @BindView(R.id.imgPreview)
    ImageView imagePreview ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_details);
        ButterKnife.bind(this);
        this.showPrescription(this.getPrescriptionModelFromIntent(this.getIntent()));
    }
    private PrescriptionModel getPrescriptionModelFromIntent(Intent intent) {
        return (PrescriptionModel) intent.getSerializableExtra("prescriptionModel");
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            this.startActivity(new Intent(this,myPrescriptions.class));
        }
        return true;
    }

    public void showPrescription(PrescriptionModel prescriptionModel){
        doctorNameTxt.setText(String.valueOf(prescriptionModel.getDoctorName()));
        prescriptionDateTxt.setText(String.valueOf(prescriptionModel.getPrescriptionDate()));
        String image = prescriptionModel.getPrescriptionImage();
        //TODO : make the imageview shows the prescription image
        //imagePreview.setImageBitmap(getBitmapFromString(image));

    }

    private Bitmap getBitmapFromString(String jsonString) {
        /*
         * This Function converts the String back to Bitmap
         * */
        byte[] decodedString = Base64.decode(jsonString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
