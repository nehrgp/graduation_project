package com.nehr.common.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Drug {
	@Id
	String id;
	@Field
	String name;
	@Field
	String effectiveSubstance;
	@Field
	float concentration;
	@Field
	boolean isDeleted;
	

	
	@PersistenceConstructor
	public Drug(@JsonProperty("id") String id,@JsonProperty("name") String name, @JsonProperty("effectiveSubstance") String effectiveSubstance,
			@JsonProperty("concentration") float concentration,@JsonProperty("isDeleted") boolean isDeleted) {
		super();
		this.id=id;
		this.name = name;
		this.effectiveSubstance = effectiveSubstance;
		this.concentration = concentration;
		this.isDeleted=isDeleted();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEffectiveSubstance() {
		return effectiveSubstance;
	}

	public void setEffectiveSubstance(String effectiveSubstance) {
		this.effectiveSubstance = effectiveSubstance;
	}

	public float getConcentration() {
		return concentration;
	}

	public void setConcentration(float concentration) {
		this.concentration = concentration;
	}
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


}
