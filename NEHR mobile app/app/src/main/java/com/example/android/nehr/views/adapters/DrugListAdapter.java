package com.example.android.nehr.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.models.DrugModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahmoud on 2/9/2018.
 */

public class DrugListAdapter extends ArrayAdapter<DrugModel> {
    public DrugListAdapter(Context context, List<DrugModel> drugs) {
        super(context, 0, drugs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DrugModel drugModel = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.drug_list, parent, false);
        }
        // Lookup view for data population
        TextView drugName = (TextView) convertView.findViewById(R.id.name);
        // Populate the data into the template view using the data object
        drugName.setText(drugModel.getName());
        // Return the completed view to render on screen
        return convertView;
    }
}
