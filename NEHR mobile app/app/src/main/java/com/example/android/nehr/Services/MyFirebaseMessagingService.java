package com.example.android.nehr.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.example.android.nehr.R;
import com.example.android.nehr.alarm.reminder.ReminderAlarmService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by mahmoud on 01/07/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService{

    private int count = 0;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
       // String message = remoteMessage.getNotification().getBody().toString();
        Map<String, String> data = remoteMessage.getData();

        Intent i = new Intent(this,com.example.android.nehr.views.activities.messageReciver.class);
        i.putExtra("MESSAGE",data.get("msg"));
        i.putExtra("doctorName", data.get("doctorName"));
        i.putExtra("doctorEmail", data.get("doctorEmail"));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);


    }

       // Log.d(TAG, "Message Received: "+remoteMessage.getNotification().getBody().toString());



    }

