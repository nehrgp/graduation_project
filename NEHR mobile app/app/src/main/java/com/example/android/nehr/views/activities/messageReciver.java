package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.data.RemoteDBClient;
import com.example.android.nehr.presenters.DoctorPermissionPresenter;
import com.example.android.nehr.presenters.DoctorPermissionPresenterImpl;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.DoctorPermissionView;
import com.example.android.nehr.alarm.AlarmMainActivity;


import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class messageReciver extends NavigationDrawer implements  DoctorPermissionView {


    @BindView(R.id.messageTxt)
    TextView messageTxt ;
    @BindView(R.id.yesBtn)Button yesBtn ;
    @BindView(R.id.noBtn) Button noBtn ;
    HashMap<String,String> data ;
    String doctorName;
    String doctorEmail;
    String message;
    RemoteDBClient dbClient = new RemoteDBClient();
    DoctorPermissionPresenter presenter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_reciver);
        ButterKnife.bind(this);
        presenter = new DoctorPermissionPresenterImpl(this);

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {

           doctorEmail = extras.getString("doctorEmail");
           doctorName = extras.getString("doctorName");
           message = extras.getString("MESSAGE");

            Log.d("message",doctorEmail+" "+doctorName);
        }

        messageTxt.setText(message);
    }



    @OnClick({R.id.yesBtn})
    public void yesBtnClicked(){
        presenter.sendAuthorizationResponse("yes",doctorEmail,SaveSharedPreference.getnId(this));
     //   dbClient.sendAuthorizationResponse("yes",doctorEmail, SaveSharedPreference.getnID(this));
        navigateToDashboard();
    }
    @OnClick({R.id.noBtn})
    public void noBtnClicked(){
        presenter.sendAuthorizationResponse("no",doctorEmail,SaveSharedPreference.getnId(this));
        //dbClient.sendAuthorizationResponse("no",doctorEmail, SaveSharedPreference.getnID(this));
        navigateToDashboard();
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void navigateToDashboard() {
        Intent i = new Intent(this, AlarmMainActivity.class);
        startActivity(i);
        finish();
    }
}
