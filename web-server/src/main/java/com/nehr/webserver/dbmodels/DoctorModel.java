package com.nehr.webserver.dbmodels;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoctorModel {

	private String email;
	private String password;

	
	public DoctorModel() {}
	
	public DoctorModel(@JsonProperty("email")String email, @JsonProperty("password")String password) {
		super();
		this.email = email;
		this.password = password;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


}
