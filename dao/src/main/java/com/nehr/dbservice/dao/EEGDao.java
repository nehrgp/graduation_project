package com.nehr.dbservice.dao;
import com.nehr.dbservice.models.EEGStreamModel;

public interface EEGDao {

	EEGStreamModel upsertData(EEGStreamModel eegModel);
	EEGStreamModel getDataById(String id);
}
	