package com.nehr.fileservice.dao;

import org.springframework.web.multipart.MultipartFile;

import com.nehr.fileservice.dto.Prescription;

public interface FileDao {

	public String uploadFile(MultipartFile multipartFile);
	public String deleteFileFromS3Bucket(String fileUrl);
	public Prescription addPrescription(Prescription prescription);
}
