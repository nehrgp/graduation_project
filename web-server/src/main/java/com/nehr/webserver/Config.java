package com.nehr.webserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Config {
	
	@Value("${nehr.dbservice.url}")
	private String dbserviceUrl;

	public String getDbserviceUrl() {
		return dbserviceUrl;
	}

	public void setDbserviceUrl(String dbserviceUrl) {
		this.dbserviceUrl = dbserviceUrl;
	}
}
