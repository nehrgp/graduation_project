package com.nehr.dbservice.models;

import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "patient")

public class Patient {

	String nID;
	String firstName;
	String email;
	String lastName;
	Integer dateDay;
	Integer dateMonth;
	Integer dateYear;
	String gender;
	String mobileNumber;
	String passwordHash;
	ArrayList<String> doctorEmail;
	
	
	@DynamoDBHashKey(attributeName = "nID")
	public String getnID() {
		return nID;
	}

	public void setnID(String nID) {
		this.nID = nID;
	}

	@DynamoDBIndexHashKey(globalSecondaryIndexName="mobileNumber-index" , attributeName = "mobileNumber")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@DynamoDBAttribute(attributeName = "firstName")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@DynamoDBAttribute(attributeName = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@DynamoDBAttribute(attributeName = "lastName")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@DynamoDBAttribute(attributeName = "dateDay")
	public Integer getDateDay() {
		return dateDay;
	}

	public void setDateDay(Integer dateDay) {
		this.dateDay = dateDay;
	}

	@DynamoDBAttribute(attributeName = "dateMonth")
	public Integer getDateMonth() {
		return dateMonth;
	}

	public void setDateMonth(Integer dateMonth) {
		this.dateMonth = dateMonth;
	}

	@DynamoDBAttribute(attributeName = "dateYear")
	public Integer getDateYear() {
		return dateYear;
	}

	public void setDateYear(Integer dateYear) {
		this.dateYear = dateYear;
	}

	@DynamoDBAttribute(attributeName = "gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@DynamoDBAttribute(attributeName = "passwordHash")
	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	@DynamoDBAttribute(attributeName = "doctorEmail")
	public ArrayList<String> getDoctorEmail() {
		return doctorEmail;
	}

	public void setDoctorEmail(ArrayList<String> doctorEmail) {
		this.doctorEmail = doctorEmail;
	}

	
}
