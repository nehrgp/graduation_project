package com.nehr.webserver.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.nehr.webserver.clients.DbserviceClient;
import com.nehr.webserver.dto.Patient;
import com.nehr.webserver.dto.UpdatePersonalInfoModel;

@Controller
@RequestMapping("/user/update/personal-info")
public class UpdatePersonalInfoContoller {

	@Autowired
	DbserviceClient dbserviceClient;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView navToUpdateInfo(HttpSession session) {
		if (session.getAttribute("loggedInUserId") == null) {
			return new ModelAndView("redirect:/");
		} else {
			ModelAndView model = new ModelAndView("personal");
	        Patient patient =dbserviceClient.getPatientByNId((String)session.getAttribute("loggedInUserId")) ;
	        model.addObject("patient",patient);
			return model;
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public String updateInfo(UpdatePersonalInfoModel updatePersonalInfoModel, HttpSession session) {
		// //ModelAndView loginPage = new ModelAndView("personal");
        Patient patient =dbserviceClient.getPatientByNId((String)session.getAttribute("loggedInUserId")) ;
		if (patient != null ) {
			patient.setDateDay(updatePersonalInfoModel.getDateDay());
			patient.setDateMonth(updatePersonalInfoModel.getDateMonth());
			patient.setDateYear(updatePersonalInfoModel.getDateYear());
			patient.setEmail(updatePersonalInfoModel.getEmail());
			patient.setFirstName(updatePersonalInfoModel.getFirstName());
			patient.setLastName(updatePersonalInfoModel.getLastName());
			patient.setMobileNumber((String) session.getAttribute("mobileNumber"));
			patient.setGender(updatePersonalInfoModel.getGender());
            dbserviceClient.updatePatientPersonalInfo(patient);

			return "updated";
		} else
			return "error";

	}
}
