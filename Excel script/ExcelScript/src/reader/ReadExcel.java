package reader;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.*;

import javax.net.ssl.HttpsURLConnection;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class ReadExcel {

    private String inputFile;

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }
    public void post(String[] params) {
    	int paramlength = params.length;
    	//for (int i = 0 ; i<paramlength;i++) {
    		try {

    			//String url = "http://18.217.96.80:8090/drug/upsert";
    			
    	        HttpClient client = HttpClientBuilder.create().build();
    	        HttpPost post = new HttpPost("http://18.217.96.80:8090/drug/upsert");

    	        // Create some NameValuePair for HttpPost parameters
    	        List<NameValuePair> arguments = new ArrayList<>(3);
    	        arguments.add(new BasicNameValuePair("id", "1551315"));
    	        arguments.add(new BasicNameValuePair("name", "Hossam"));
    	        arguments.add(new BasicNameValuePair("description", "Administrator"));
                post.setEntity(new UrlEncodedFormEntity(arguments));
                HttpResponse response = client.execute(post);

                // Print out the response message
                System.out.println(EntityUtils.toString(response.getEntity()));

    		}
 
    	catch (IOException e) {   
    	    // openConnection() failed
    	    // ...
    		System.out.println("failed2");
    	}
    	}
    	
    //}

    public void read() throws IOException  {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        try {
            w = Workbook.getWorkbook(inputWorkbook);
            // Get the first sheet
            Sheet sheet = w.getSheet(0);
          
            int RowNumber = sheet.getRows();
            int ColNumber = sheet.getColumns();
            
            String[] params = new String[RowNumber];
            
            for (int i = 0; i < RowNumber; i++) {
                for (int j = 0; j < ColNumber; j++) {
                    Cell cell = sheet.getCell(j, i);
                    params[j] = cell.getContents();
                    }
              post(params);
                } 
        }
         catch (BiffException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
        ReadExcel test = new ReadExcel();
        test.setInputFile(("ExcelSheet.xls"));
       test.read();
    }

} 

    