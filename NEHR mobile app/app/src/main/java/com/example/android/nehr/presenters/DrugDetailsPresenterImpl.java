package com.example.android.nehr.presenters;

import com.example.android.nehr.interactors.DrugInteractor;
import com.example.android.nehr.interactors.DrugInteractorImpl;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.DrugDetailsView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahmoud on 2/10/2018.
 */

public class DrugDetailsPresenterImpl implements DrugDetailsPresenter {

    DrugDetailsView view ;
    DrugInteractor interactor;

    public DrugDetailsPresenterImpl(DrugDetailsView view)
    {
        this.view = view;
        interactor=new DrugInteractorImpl();
    }

    @Override
    public void addToMyDrugs(DrugModel drug) {
        PatientDrugHistory patientDrugHistory= this.fromDrugToPatientDrugHistory(drug,"not set","not set");
        interactor.callAddDrugAPI(this,patientDrugHistory);
    }

    @Override
    public void drugAddedSuccesfully() {
        view.drugAddedSuccess();

    }

    @Override
    public void drugAddingFailed() {
        view.drugAddFailed();

    }


    private PatientDrugHistory fromDrugToPatientDrugHistory(DrugModel drug,String startDate,String endDate){
        PatientDrugHistory patientDrugHistory=new PatientDrugHistory();
        patientDrugHistory.setnId(SaveSharedPreference.getnId(view.getContext()));

        DrugHistory drugHistory=new DrugHistory();
        drugHistory.setName(drug.getName());
        drugHistory.setStartDate(startDate);
        drugHistory.setEndDate(endDate);
        drugHistory.setImage(drug.getImage());

        List<DrugHistory> drugHistoryList=new ArrayList<DrugHistory>();
        drugHistoryList.add(drugHistory);
        patientDrugHistory.setDrugs(drugHistoryList);
        return patientDrugHistory;

    }


}
