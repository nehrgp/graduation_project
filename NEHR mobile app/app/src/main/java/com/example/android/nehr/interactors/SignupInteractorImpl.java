package com.example.android.nehr.interactors;



import android.util.Log;

import com.example.android.nehr.data.DBClientImpl;
import com.example.android.nehr.data.DBClient;
import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.presenters.SignupPresenter;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by omar on 11/28/2017.
 */

public class SignupInteractorImpl implements SignupInteractor {

    DBClient dbClient ;
    Observer<SignupModel> observer ;

    public SignupInteractorImpl() {

        this.dbClient = new DBClientImpl();
    }


    @Override
    public void callSignupAPI(SignupPresenter signupPresenter, SignupModel signupModel) {

        observer=new Observer<SignupModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(SignupModel returnedSignupModel) {
                if (returnedSignupModel != null) {
                    signupPresenter.signupSuccess(returnedSignupModel);
                } else {
                    signupPresenter.signupFailure();
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d("ourMsg", "maybeNetworkError");
                Log.e("Error",e.getMessage());
                signupPresenter.signupFailure();

            }

            @Override
            public void onComplete() {

            }
        };

        dbClient.upsertBasicInfo(signupModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);



    }
}
