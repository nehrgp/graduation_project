package com.nehr.webserver.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class SignupModel {

	@NotEmpty
	String nID;
	
	@NotEmpty
	String mobileNumber;
	@NotEmpty
	String password;
	@NotEmpty
	String passwordRepeat;

	public SignupModel(){};
	public SignupModel( String mobileNumber,String password) {
		super();
		this.mobileNumber = mobileNumber;
		this.password = password;
	}
	
	

	
	public String getnID() {
		return nID;
	}
	public void setnID(String nID) {
		this.nID = nID;
	}
	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordRepeat() {
		return passwordRepeat;
	}
	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	

}
