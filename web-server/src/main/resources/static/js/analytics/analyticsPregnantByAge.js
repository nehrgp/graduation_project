function drawPregnantByAge(){
	// Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    // 'http://localhost:8080/analytics//object'
    function drawChart() {

      // Create the data table.
    	var dataPregnantByAge = new google.visualization.DataTable();

    	console.log("PregnantByAge");
    	
    	req=new XMLHttpRequest();
  		req.open("GET",'http://18.191.180.56:8013/analytics/pregnants/age/',true); // the analytics api
  		req.send();
  		
  		req.onload = function(){
  			
  			json_object_pregnantAge =JSON.parse(req.responseText);
  			console.log(JSON.stringify(json_object_pregnantAge));
	  		dataPregnantByAge.addColumn('string', "Category");
	  		dataPregnantByAge.addColumn('number', "Count");
		
	  		//object_age = json_object_age.data;
	  		//console.log(JSON.stringify(object_age));
	  		items = [];
	  		for(var key in json_object_pregnantAge){
	  			item = [];
	  			item.push(key);
	  			item.push(json_object_pregnantAge[key]);
	  			items.push(item);
	  		}
	  		dataPregnantByAge.addRows(items);
	  		
	  		// Set chart options
	        var options = {'title':'Pregnant By Age',
	        		 //pieHole: 0.4,
                    'width':1200,
                    'height':900};
	        // Instantiate and draw our chart, passing in some options.
	        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        chart.draw(dataPregnantByAge, options);
  		}      
    }
}

document.addEventListener('DOMContentLoaded',function(){
	$("#pregnantByAge").click(drawPregnantByAge);
});