package com.nehr.dbservice.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.AuthenticDoctorDao;
import com.nehr.dbservice.models.AuthenticDoctor;

@RestController
@RequestMapping("/authenticDoctor")
public class AuthenticDoctorController {

	@Autowired
	AuthenticDoctorDao authenticDoctorDao;
	
	@GetMapping("/find/nID/{doctorEmail}")
	public AuthenticDoctor findAuthenticDoctorById(@PathVariable("doctorEmail") String doctorEmail) {
		return authenticDoctorDao.findPatientID(doctorEmail);
	}
	
	@PostMapping(path = "/add/nID", consumes = "application/json")
	public AuthenticDoctor addDoctorEmail(@RequestBody AuthenticDoctor authenticDoctor) {
		return authenticDoctorDao.addPatientID(authenticDoctor);
	}
	
	@GetMapping(path = "/find/nID/{doctorEmail}/{nID}")
	public boolean isAuthentic(@PathVariable("nID") String patient_id ,@PathVariable("doctorEmail") String doctor_email) {
		return authenticDoctorDao.isAuthentic(patient_id, doctor_email);
	}
}
