package com.nehr.webserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nehr.webserver.clients.DbserviceClient;
import com.nehr.webserver.dto.Drug;

@Controller
public class DrugController {

	@Autowired
	DbserviceClient dbserviceClient;

	@RequestMapping(value = "/drug/show-all", method = RequestMethod.GET)
	public ModelAndView showAllDrugs() {
		List<Drug> drugs = dbserviceClient.findAllDrugs();
		ModelAndView model = new ModelAndView("drugs/drugs");
		model.addObject("drugs", drugs);
		return model;
	}
	
	@RequestMapping(value = "/drug/find/prefix", method = RequestMethod.GET)
	public ModelAndView getDrugsByPrefix(@RequestParam("prefix") String prefix) {	
		if (prefix == "") {
			List<Drug> drugs = dbserviceClient.findAllDrugs();
			ModelAndView model = new ModelAndView("drugs/drugs");
			model.addObject("drugs", drugs);
			return model;
		} else {
			List<Drug> drugs = dbserviceClient.getDrugsByPrefix(prefix);
			ModelAndView model = new ModelAndView("drugs/drugs");
			drugs = dbserviceClient.getDrugsByPrefix(prefix);
			model.addObject("drugs", drugs);
			return model;
		}
	}

	@RequestMapping(value = "/drug", method = RequestMethod.GET)
	public ModelAndView getDrugsView() {
		List<Drug> drugs = dbserviceClient.findAllDrugs();
		ModelAndView model = new ModelAndView("drugs/drugs");
		model.addObject("drugs", drugs);
		return model;
	}
}
