function drawSmoker(){
	// Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawSmokerChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    // 'http://localhost:8080/analytics//object'
    function drawSmokerChart() {

      // Create the data table.
    	var data = new google.visualization.DataTable();

    	console.log("SmokerGender");
    	
    	req=new XMLHttpRequest();
  		req.open("GET",'http://18.191.180.56:8013/analytics/patientsSlides/smokers/gender',true); // the analytics api
  		req.send();
  	
  		req.onload = function(){
  			
  			json_object_smoker=JSON.parse(req.responseText);
  			console.log(JSON.stringify(json_object_smoker));
	  		data.addColumn('string', "Category");
	  		data.addColumn('number', "Count");
			  		
	  		items = [];
	  		for(var key in json_object_smoker){
	  			item = []
	  			item.push(key);
	  			item.push(json_object_smoker[key]);
	  			items.push(item);
	  		}
	  		data.addRows(items);
	  		
	  		// Set chart options
	        var options = {'title':'Smokers Gender',
	        		 //pieHole: 0.4,
                    'width':1200,
                    'height':900};
	        // Instantiate and draw our chart, passing in some options.
	        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        chart.draw(data, options);
  		}      
    }
}

document.addEventListener('DOMContentLoaded',function(){
	$("#smoker").click(drawSmoker);
});