package com.example.android.nehr.presenters;


import com.example.android.nehr.models.SignupModel;

/**
 * Created by omar on 11/28/2017.
 */

public interface SignupPresenter {
    void attmeptSignup(SignupModel signupModel);
    void signupSuccess(SignupModel signupModel);
    void signupFailure();

}
