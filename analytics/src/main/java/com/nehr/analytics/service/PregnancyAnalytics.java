package com.nehr.analytics.service;

import java.util.Map;

public interface PregnancyAnalytics {
	public Map<String,Integer> getPregnantsByAge();
	public Map<String,Integer> getAlcholicPrgnants();
	public Map<String,Integer> getSmokerPregnants();
	public Map<String,Integer> getCleanPregnants();
	public Map<String,Integer> getAlcholicSmokerPregnats();
	
}
