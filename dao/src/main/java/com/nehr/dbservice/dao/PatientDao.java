package com.nehr.dbservice.dao;


import com.nehr.dbservice.models.Patient;

public interface PatientDao {
	Patient findUserByNationalId (String nID);
	Patient upsertUser (Patient user);
	Patient findUserByMobileNumber(String mobileNumber);
	
	
}
