package com.nehr.analytics.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class PatientAnalyticsImpl implements PatientAnalytics {

	static final String DB_URL = "jdbc:redshift://data-warehouse.copyte5s6ypv.us-east-2.redshift.amazonaws.com:5439/dev";
	static final String USER = "hossam26644";
	static final String PASS = "4176532Ab";

	@Override
	public Map<String, Integer> getPatientsSlides() {
		
		int sum=0;

		String FirstSector, SecondSector, ThirdSector, FourthSector, FifthSector, sixthSector;
		int year = Calendar.getInstance().get(Calendar.YEAR);

		String query = String.valueOf(year - 10);
		FirstSector = getPopulationPercentageQuery(query);
		int FirstSegment = Integer.parseInt(FirstSector);
		sum+=FirstSegment;

		query = String.valueOf(year - 20);
		SecondSector = getPopulationPercentageQuery(query);
		int SecondSegment = Integer.parseInt(SecondSector) - sum;
		sum+=SecondSegment;

		query = String.valueOf(year - 30);
		ThirdSector = getPopulationPercentageQuery(query);
		int ThirdSegment = Integer.parseInt(ThirdSector) - sum;
		sum+=ThirdSegment;

		query = String.valueOf(year - 40);
		FourthSector = getPopulationPercentageQuery(query);
		int FourthSegment = Integer.parseInt(FourthSector) - sum;
		sum+=FourthSegment;

		query = String.valueOf(year - 50);
		FifthSector = getPopulationPercentageQuery(query);
		int FifthSegment = Integer.parseInt(FifthSector) - sum;
		sum+=FifthSegment;

		query = String.valueOf(year - 120);
		sixthSector = getPopulationPercentageQuery(query);
		int sixthSegment = Integer.parseInt(sixthSector) - sum;
		
		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("10", FirstSegment);
		slides.put("20", SecondSegment);
		slides.put("30", ThirdSegment);
		slides.put("40", FourthSegment);
		slides.put("50", FifthSegment);
		slides.put("120", sixthSegment);

		return slides;
	}

	@Override
	public Map<String, Integer> getSmokersPercentages() {
		String malesNumber;
		malesNumber = getSmokersPercentageQuery("'male'");
		String femalesNumber;
		femalesNumber = getSmokersPercentageQuery("'female'");
		HashMap<String, Integer> slides = new HashMap<>();
		slides.put("males", Integer.parseInt(malesNumber));
		slides.put("females",Integer.parseInt(femalesNumber));
		return slides;
	}

	private static String getPopulationPercentageQuery(String PastYears) {
		Connection conn = null;
		Statement stmt = null;
		String Query;

		Query = "select count(nid) from patientanalytics WHERE dateyear >=" + PastYears;

		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.amazon.redshift.jdbc.Driver");

			// STEP 3: Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(Query);
			while (rs.next()) {
				// Retrieve two columns.
				String result = rs.getString("count");

				// Display values.
				return result;
			}

			stmt.close();
			conn.close();
			rs.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return Query;

	}

	private static String getSmokersPercentageQuery(String gender) {
		Connection conn = null;
		Statement stmt = null;
		String Query;
		Query = "select count(nid) from patientanalytics WHERE smoker=1 and gender=" + gender;

		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.amazon.redshift.jdbc.Driver");

			// STEP 3: Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(Query);
			while (rs.next()) {
				// Retrieve two columns.
				String result = rs.getString("count");

				// Display values.
				return result;
			}

			stmt.close();
			conn.close();
			rs.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return Query;

	}

}
