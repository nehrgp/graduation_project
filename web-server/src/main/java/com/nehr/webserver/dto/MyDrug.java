package com.nehr.webserver.dto;

public class MyDrug {
	String name;
	String startDate;
	
	String endDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDateDay) {
		this.endDate = endDateDay;
	}
	
	
}
