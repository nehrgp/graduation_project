package com.nehr.dbservice.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "patient")
public class MobileInstance {
	
	String mobileInstanceIdToken;
	String nID;


	
	@DynamoDBHashKey(attributeName = "nID")
	public String getnId() {
		return nID;
	}

	public void setnId(String nID) {
		this.nID = nID;
	}
	
	
	
	@DynamoDBAttribute(attributeName = "mobileInstanceIdToken")
	public String getMobileInstanceIdToken(){
		return mobileInstanceIdToken;
	}

	public void setMobileInstanceIdToken(String mobileInstanceIdToken) {
		this.mobileInstanceIdToken = mobileInstanceIdToken;
	}

}
