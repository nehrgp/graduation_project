package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.presenters.DrugPresenter;
import com.example.android.nehr.presenters.MyDrugPresenterImpl;
import com.example.android.nehr.views.adapters.MyDrugListAdapter;
import com.example.android.nehr.views.views.MyDrugsView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class myDrugsActivity extends NavigationDrawer implements MyDrugsView {

    @BindView(R.id.myDrugListView) ListView myDrugListView ;
    @BindView(R.id.addDrugBtn)
    FloatingActionButton addDrugBtn ;

    DrugHistory drugHistories = new DrugHistory();
    DrugPresenter presenter = new MyDrugPresenterImpl(this) ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydrugs);
        ButterKnife.bind(this);
        presenter.getMyDrugs();
    }



    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    @Override
    public void showDrugs(List<DrugHistory> drugHistory) {
        MyDrugListAdapter adapter = new MyDrugListAdapter(this,drugHistory);
        myDrugListView.setAdapter(adapter);
        myDrugListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),myDrugHistoryDetails.class);
                drugHistories.setName(adapter.getItem(position).getName());
                drugHistories.setStartDate(adapter.getItem(position).getStartDate());
                drugHistories.setEndDate(adapter.getItem(position).getEndDate());
                drugHistories.setImage(adapter.getItem(position).getImage());
                intent.putExtra("drugHistory",drugHistories);
                startActivity(intent);
            }
        });

    }
    @OnClick(R.id.addDrugBtn)
    public void addDrugBtnClicked(){
        Intent i = new Intent(myDrugsActivity.this, alldrugsActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void fail() {
        Toast.makeText(myDrugsActivity.this,"fail",Toast.LENGTH_SHORT).show();
    }
}
