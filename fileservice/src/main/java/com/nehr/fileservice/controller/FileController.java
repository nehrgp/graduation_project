package com.nehr.fileservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nehr.fileservice.config.AmazonClient;
import com.nehr.fileservice.dao.FileDao;
import com.nehr.fileservice.dto.Prescription;


@RestController
@RequestMapping("/storage")
public class FileController {

	@Autowired
	FileDao fileDao;
	
    private AmazonClient amazonClient;

    @Autowired
    FileController(AmazonClient amazonClient) {
        this.amazonClient = amazonClient;
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestPart(value = "file") MultipartFile file) {
        return fileDao.uploadFile(file);
    }


    @DeleteMapping("/deleteFile")
    public String deleteFile(@RequestPart(value = "url") String fileUrl) {
        return fileDao.deleteFileFromS3Bucket(fileUrl);
    }
    
    @PostMapping("/addPrescription")
    public Prescription addPrescription(@ModelAttribute Prescription prescription) {
		return fileDao.addPrescription(prescription);
    }
    
}