package com.example.android.nehr.interactors;


import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.presenters.LoginPresenter;

/**
 * Created by omar on 1/30/2018.
 */

public interface LoginInteractor {
    void callLoginAPI(LoginPresenter loginPresenter, LoginModel loginModel);
}
