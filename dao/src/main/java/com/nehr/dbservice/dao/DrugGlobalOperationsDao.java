package com.nehr.dbservice.dao;

import java.util.List;

import com.nehr.dbservice.models.Drug;

public interface DrugGlobalOperationsDao {
	void deActivateDrug(String id);
	List<Drug> getDrugsByPrefix(String prefix);
	List<Drug> getDrugsByEffectiveMaterial(String effectiveMaterial);
	List<Drug> getDrugsBySymptom(String symptom);
	List<Drug> getDrugsByDisease(String disease);
	
}
