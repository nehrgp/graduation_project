*Ubuntu*

*Connect to EC2*
<do these steps for the first time only>
1- go to the directory of nehr.pem
2- open the terminal 
3- write this command -> chmod 400 ~/nehr.pem // this command makes the file only readable by you
<for each time>
4- write this command -> ssh -i nehr.pem ec2-user@18.191.180.56
5- in case of "can't be established ESCDA" write -> yes
6- optinal ... you can install mvn and git
7- write your commands
*******
*kill jar process*
to get process number -> ps aux
then -> Kill - 9 PROCESSESnumber

to run mvn -> nohup mvn spring-boot:run&
to run jar -> nohup java -jar <packageName.jar>&

note: nohup <command>& .... to separete the process to background 
*********

*Upload files*
1- https://stackoverflow.com/questions/16744863/connect-to-amazon-ec2-file-directory-using-filezilla-and-sftp

**************
**************
**************


*Windows*

https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html

1- install PuTTY  https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
2- install winSCP https://winscp.net/eng/download.php
3- in the terminal wirte after login as -> ec2-user




*Generate Key File*
1- open putty key gen
2- load key
3- save private key as ppk file

4- open putty
5- write IP
6- go to ssh -> auth -> browse ppk file then click ok


*to transfer file .... usin winscp*
1- open winscp 
2- in hostname write -> <your instance ip>
3- in username write -> ec2-user
4- click advanced
5- then ssh -> authantication -> browse private ppk file
6- save and click login then yes
