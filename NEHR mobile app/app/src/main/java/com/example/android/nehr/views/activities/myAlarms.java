package com.example.android.nehr.views.activities;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.sessions.SaveSharedPreference;
//import com.example.android.nehr.views.alarm.AlarmNotificationService;
//import com.example.android.nehr.views.alarm.AlarmSoundService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class myAlarms extends NavigationDrawer {

    @BindView(R.id.adherenceTxt)TextView adherenceTxt ;
    String adherenceNumberStringNum ;
    String adherenceNumberStringDen ;

    int adherenceNumberDen ;
    int adherenceNumberNum ;

    private PendingIntent pendingIntent;
    private String drugName ="";


    private void navigateToAdherence() {
        Intent i = new Intent(this, AdherenceActivity.class);
        startActivity(i);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_alarms);
        ButterKnife.bind(this);
          Bundle data=getIntent().getExtras();
            drugName = data.get("drugName").toString();
        if(SaveSharedPreference.getAdherence(getApplicationContext(),drugName+"Den").length()!= 0){
            adherenceNumberStringNum = SaveSharedPreference.getAdherence(getApplicationContext(),drugName+"Num");
            adherenceNumberStringDen = SaveSharedPreference.getAdherence(getApplicationContext(),drugName+"Den");

            adherenceNumberNum = Integer.parseInt(adherenceNumberStringNum);
            adherenceNumberDen = Integer.parseInt(adherenceNumberStringDen);

        }
        else
        {
            adherenceNumberNum=0;
            adherenceNumberDen=0;

        }

        adherenceNumberDen+=1;
        SaveSharedPreference.setAdherence(getApplicationContext(),drugName+"Den",String.valueOf(adherenceNumberDen));


        AlertDialog.Builder alert = new AlertDialog.Builder(myAlarms.this);
        alert.setMessage("Did you take your medication ? ").setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        adherenceNumberNum+=1;
                        adherenceTxt.setText(String.valueOf(adherenceNumberNum));
                        SaveSharedPreference.setAdherence(getApplicationContext(),drugName+"Num",String.valueOf(adherenceNumberNum));

                        dialogInterface.cancel();
                        navigateToAdherence();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        adherenceTxt.setText(String.valueOf(adherenceNumberNum));
                        SaveSharedPreference.setAdherence(getApplicationContext(),drugName+"Num",String.valueOf(adherenceNumberNum));                       dialogInterface.cancel();
                       navigateToAdherence();
                    }
                });
            AlertDialog alertDialog = alert.create();
            alertDialog.setTitle(drugName+" adherence");
            alertDialog.show();

    }



  /*  public void stopAlarm(){
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);//cancel the alarm manager of the pending intent


        //Stop the Media Player Service to stop sound
        stopService(new Intent(myAlarms.this, AlarmSoundService.class));

        //remove the notification from notification tray
        NotificationManager notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(AlarmNotificationService.NOTIFICATION_ID);

        Toast.makeText(this, "Alarm Canceled/Stop by User.", Toast.LENGTH_SHORT).show();
    }*/


}
