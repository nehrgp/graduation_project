package com.nehr.analytics.service;

import java.util.Map;

public interface PatientAnalytics {
	
	Map<String, Integer> getPatientsSlides();
	Map<String, Integer> getSmokersPercentages();
	
}
