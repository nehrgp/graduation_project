package com.nehr.webserver.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nehr.webserver.MailConfig;
import com.nehr.webserver.dbmodels.EmailModel;

@Component
public class MailserviceClient {

	@Autowired
	MailConfig mailConfig ;
	
	public void sendSimpleEmail(EmailModel email){
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<EmailModel> request = new HttpEntity<>(email);
		restTemplate.postForObject(mailConfig.getMailserviceUrl() + "email/send/simpleEmail", request, EmailModel.class);
	}
	
	public void sendHTMLEmail(EmailModel email){
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<EmailModel> request = new HttpEntity<>(email);
		restTemplate.postForObject(mailConfig.getMailserviceUrl() + "email/send/htmlEmail", request, EmailModel.class);
	}
}
