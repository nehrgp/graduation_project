package com.nehr.dbservice.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.nehr.dbservice.models.DrugHistory;
import com.nehr.dbservice.models.PatientDrugHistory;

@Repository
public class PatientDrugHistoryDaoImpl implements PatientDrugHistoryDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;

	@Override
	public PatientDrugHistory insertDrug(PatientDrugHistory patientDrugHistory) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		try {
			
			PatientDrugHistory currentHistory =this.findPatientDrugHistory(patientDrugHistory.getnId());
			
			List<DrugHistory> updatedDrugList=currentHistory.getDrugs();
			if(updatedDrugList == null) {
				updatedDrugList = new ArrayList<DrugHistory>();
			}
			//updatedDrugList.addAll(patientDrugHistory.getDrugs());
			for(int i=0; i<patientDrugHistory.getDrugs().size(); i++) {
				DrugHistory dh = patientDrugHistory.getDrugs().get(i);
				dh.setName(dh.getName().toLowerCase()); 
				updatedDrugList.add(dh);
			}
			patientDrugHistory.setDrugs(updatedDrugList);
			mapper.save(patientDrugHistory, new DynamoDBMapperConfig(SaveBehavior.APPEND_SET));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return this.findPatientDrugHistory(patientDrugHistory.getnId());
		}
	}

	@Override
	public void deleteDrugByName(String patientId ,String drugName) {
		PatientDrugHistory patientDrugHistory = this.findPatientDrugHistory(patientId);
		if (patientDrugHistory !=null){
			List<DrugHistory> drugs=patientDrugHistory.getDrugs();
			for(int i=0;i<drugs.size();i++){
				DrugHistory drug=drugs.get(i);
				if(drug.getName().equals(drugName)){
					drugs.remove(i);
					patientDrugHistory.setDrugs(drugs);
					break;
				}
				
			}
			
			DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
			try {
				mapper.save(patientDrugHistory);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
	}

	@Override
	public PatientDrugHistory findPatientDrugHistory(String patientId) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		PatientDrugHistory patientDrugHistory = new PatientDrugHistory();
		patientDrugHistory.setnId(patientId);
		PatientDrugHistory result = null;
		try {
			result = mapper.load(patientDrugHistory);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;

		}
	}

	@Override
	public boolean contradict(String drug1, String drug2) {
		// TODO Auto-generated method stub
		return false;
	}


}
