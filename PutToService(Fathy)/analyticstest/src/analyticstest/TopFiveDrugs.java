package analyticstest;

import java.awt.List;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

import com.amazon.redshift.jdbc.Driver;
public class TopFiveDrugs {
	
	static final String JDBC_DRIVER = "https://s3.amazonaws.com/redshift-downloads/drivers/jdbc/1.2.12.1017/RedshiftJDBC42-1.2.12.1017.jar";  
	static final String DB_URL = "jdbc:redshift://data-warehouse.copyte5s6ypv.us-east-2.redshift.amazonaws.com:5439/dev";
	static final String USER = "hossam26644";
	static final String PASS = "4176532Ab";
	
	static String Disease = "cold";
	
	public static void main(String[] args) {
	
		  Hashtable<String, String> FreqDrugsForDisease  = new Hashtable<String, String>();
		String DiseaseName = "'" + Disease+"'";
		FreqDrugsForDisease = DrugsFromDiseasQuery(DiseaseName);
		System.out.println(FreqDrugsForDisease);
}


public static Hashtable<String, String> DrugsFromDiseasQuery(String DiseaseName) {
	Connection conn = null;
	Statement stmt = null;
	String Query;
	
	Query = "SELECT drugs, COUNT(drugs) AS Drugs_Occurence FROM Patientanalytics"
	+ "	Inner JOIN druganalytics ON patientanalytics.drugs=druganalytics.name"
	+ "	where druganalytics.diseases = " + DiseaseName
	+ " Group BY patientanalytics.drugs"
	+ "	ORDER BY Drugs_Occurence DESC"
	+ "	LIMIT 5;";
	
	System.out.println(Query);
	
	try{
		  //STEP 2: Register JDBC driver
		  Class.forName("com.amazon.redshift.jdbc.Driver");

		  //STEP 3: Open a connection
		  conn = DriverManager.getConnection(DB_URL,USER,PASS);

		  //STEP 4: Execute a query
		  stmt = conn.createStatement();
		  Hashtable<String, String> ResultTable  = new Hashtable<String, String>();
		  
		  
		  ResultSet rs = stmt.executeQuery(Query);
           while(rs.next()){
               //Retrieve two columns.
               String DrugName = rs.getString("drugs");
               String Occurences = rs.getString("drugs_occurence");
               
               ResultTable.put(DrugName, Occurences);
               

               //Display values.

            }
		  
		  stmt.close();
		  conn.close();
		 rs.close();
         return ResultTable;

		}catch(SQLException se){
		  //Handle errors for JDBC
		  se.printStackTrace();
		}catch(Exception e){
		  //Handle errors for Class.forName
		  e.printStackTrace();
		}finally{
		  try{
		     if(stmt!=null)
		        stmt.close();
		  }catch(SQLException se2){
		  }// nothing we can do
		  try{
		     if(conn!=null)
		        conn.close();
		  }catch(SQLException se){
		     se.printStackTrace();
		  }//end finally try
		}//end try
	return null;
}
}