package com.nehr.dbservice.dao;

import java.util.List;

import com.nehr.dbservice.models.Drug;

public interface DrugCrudDao {
	Drug upsertDrug (Drug drug);
	Drug findDrugById(String id);
	Drug findDrugByName (String name);
	void deleteDrugById(String id);
	List<Drug> findAll();
	
}
