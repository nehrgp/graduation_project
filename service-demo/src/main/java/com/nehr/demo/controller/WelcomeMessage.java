package com.nehr.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeMessage {

	@RequestMapping("/graduation_project")
	public String sayHi() {
		return "Hi Kakashi Team In Your Spring Boot Application";
	}
}
