package com.nehr.dbservice.dao;

import com.nehr.dbservice.models.MobileInstance;

public interface MobileInstanceDao {
	public void setMobileInstanceId(String nId,String InstanceIdToken);
	public MobileInstance getMobileInstance(String nId);

}
