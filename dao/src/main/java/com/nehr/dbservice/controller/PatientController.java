package com.nehr.dbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.PatientDao;
import com.nehr.dbservice.models.Patient;

@RestController
@RequestMapping("/patient")
public class PatientController {
	@Autowired
	PatientDao patientDao;

	@RequestMapping(value = "/find/nid/{NID}", method = RequestMethod.GET)
	public Patient findUserByNID(@PathVariable("NID") String nID) {
		return patientDao.findUserByNationalId(nID);
	}

	@RequestMapping(value = "/find/mobileNumber/{mobileNumber}", method = RequestMethod.GET)
	public Patient findUserByMobileNumber(@PathVariable("mobileNumber") String mobileNumber) {
		return patientDao.findUserByMobileNumber(mobileNumber);
	}

	@RequestMapping(value = "/upsert", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Patient upsertPatient(@RequestBody Patient patient) {
		return patientDao.upsertUser(patient);
	}

}
