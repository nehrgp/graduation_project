package com.example.android.nehr.Services;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.android.nehr.data.DBClient;
import com.example.android.nehr.data.DBClientImpl;
import com.example.android.nehr.data.RemoteDBClient;
import com.example.android.nehr.models.MobileInstance;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;

/**
 * Created by mahmoud on 17/06/18.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        RemoteDBClient dbClient = new RemoteDBClient();
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Log.d(TAG,"nid from sharedpref "+SaveSharedPreference.getnId(this));

        MobileInstance mobileInstance = new MobileInstance();
        mobileInstance.setnId(SaveSharedPreference.getnId(this));
        mobileInstance.setMobileInstanceIdToken(refreshedToken);

        dbClient.saveToken(mobileInstance);

    }
}
