package com.nehr.webserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusinessClient {
	String businessClientType;
	String userName;
	String email;
	String passwordHash;
	
	public BusinessClient(){}
	public BusinessClient(@JsonProperty("businessClientType") String businessClientType, @JsonProperty("userName") String userName,
			@JsonProperty("email") String email, @JsonProperty("passwordHash") String passwordHash) {
		super();
		this.businessClientType = businessClientType;
		this.userName = userName;
		this.passwordHash = passwordHash;
		this.email = email;
	}
	
	public String getBusinessClientType() {
		return businessClientType;
	}
	public void setBusinessClientType(String businessClientType) {
		this.businessClientType = businessClientType;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
 
}
