package com.nehr.analytics.models;

import java.util.HashMap;

public class PatientsDividedByAge {

	HashMap<String, Integer> patientsSlides = new HashMap<String, Integer>();

	public HashMap<String, Integer> getPatientsSlides() {
		return patientsSlides;
	}

	public void setPatientsSlides(HashMap<String, Integer> patientsSlides) {
		this.patientsSlides = patientsSlides;
	}

}
