package com.example.android.nehr.interactors;


import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.presenters.SignupPresenter;

/**
 * Created by omar on 11/28/2017.
 */

public interface SignupInteractor {
    void callSignupAPI(SignupPresenter signupPresenter, SignupModel signupModel);

}
