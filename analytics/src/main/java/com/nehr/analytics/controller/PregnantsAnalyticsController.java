package com.nehr.analytics.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.analytics.models.AnalyticsModel;
import com.nehr.analytics.service.PregnancyAnalytics;

@RestController
public class PregnantsAnalyticsController {

	@Autowired
	PregnancyAnalytics pregnancyAnalytics;

	@RequestMapping(value = "/analytics/pregnants/age/", method = RequestMethod.GET)
	public Map<String,Integer> getSlidesByAge() {
		return pregnancyAnalytics.getPregnantsByAge();
	}
	
	@RequestMapping(value = "/analytics/pregnants/alcholic/", method = RequestMethod.GET)
	public Map<String,Integer> getAlcholic() {
		return pregnancyAnalytics.getAlcholicPrgnants();
	}
	
	@RequestMapping(value = "/analytics/pregnants/smoker/", method = RequestMethod.GET)
	public AnalyticsModel getSmoker() {
		AnalyticsModel model = new AnalyticsModel();
		model.setData(pregnancyAnalytics.getSmokerPregnants());
		return model;
	}
	
	
	@RequestMapping(value = "/analytics/pregnants/clean/", method = RequestMethod.GET)
	public Map<String,Integer> getCleanPregnants() {
		return pregnancyAnalytics.getCleanPregnants();
	}
	
	@RequestMapping(value = "/analytics/pregnants/alcoholic-smoker/", method = RequestMethod.GET)
	public Map<String,Integer> getSmokerAndAlcoholic() {
		return pregnancyAnalytics.getAlcholicSmokerPregnats();
	}
}
