package com.nehr.fcm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.fcm.fcm.FirebaseResponse;
import com.nehr.fcm.fcm.PushNotificationService;
import com.nehr.fcm.fcm.dto.Notification;
import com.nehr.fcm.fcm.dto.Push;

/**
 * 
 * Rest services for tests.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@RestController
public class ApiRestService {

	/**
	 * 
	 */
	@Autowired
	private PushNotificationService pushNotification;

	/**
	 * 
	 */
	
	

	/**
	 * send notificatin 
	/**
	 * Send to single app
	 * 
	 * @return
	 */
	@RequestMapping(value = "/push/{token}", method = RequestMethod.POST)
	public Integer push(@PathVariable("token") String token) {

			Notification notification = new Notification("default", "My app", "ya rb twsl");
			Push push = new Push(token, "high", notification);
			FirebaseResponse response  = pushNotification.sendNotification(push);
			return response.getSuccess();
		
		
	}
}
