package com.nehr.webserver.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.nehr.webserver.clients.DbserviceClient;
import com.nehr.webserver.clients.MailserviceClient;
import com.nehr.webserver.dbmodels.DoctorModel;
import com.nehr.webserver.dbmodels.EmailModel;
import com.nehr.webserver.dbmodels.PushNotification;
import com.nehr.webserver.dto.AuthenticDoctor;
import com.nehr.webserver.dto.BusinessClient;
import com.nehr.webserver.dto.Patient;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

	@Autowired
	DbserviceClient dbserviceClient;
	
	@Autowired
	MailserviceClient mailserviceClient;
	
	@GetMapping(value = "/login")
	public String loginPage() {
		return "businessClients/doctor/login";
	}
	
	@GetMapping("/")
	public String doctorIndex(HttpSession session) {
		
		if(session.getAttribute("loggedInDoctorName") != null) {
			return "redirect:/doctor/home/"+(String)session.getAttribute("loggedInDoctorName");
		} else {
			return "redirect:/doctor/login";
		}
		
	}
	
	
    @PostMapping("/login")
    public String doctorLogin(DoctorModel DoctorModel,HttpSession session) {
    	
    	BusinessClient doctor = dbserviceClient.getBusinessClientByemail(DoctorModel.getEmail()) ;
		if (doctor != null) {	
			if (DoctorModel.getPassword().equals(doctor.getPasswordHash()) ) {
				session.setAttribute("loggedInDoctorName", doctor.getUserName());
				session.setAttribute("loggedInDoctorEmail", doctor.getEmail());
				session.setAttribute("loggedInUserType", "DOCTOR");
				return "redirect:/doctor/home/"+doctor.getUserName();
			} else {
				return "passwords-don't-match";
			}
		}
		else
			return "login";
    }
    
    @GetMapping("/home/{name}")
    public String doctorHmoePage(HttpSession session){
		return "businessClients/doctor/home";
    }
    
    @GetMapping("/patientList")
    public ModelAndView patientList(HttpSession session) {
    	
    	if(session.getAttribute("loggedInDoctorName") != null) {
    		ModelAndView model = new ModelAndView("businessClients/doctor/patientList");
    		
    		String doctorEmail = (String) session.getAttribute("loggedInDoctorEmail");
    		
    		AuthenticDoctor authenticDoctor = dbserviceClient.findPatiendNIDByAuthenticDoctorEmail(doctorEmail);
    		
    		ArrayList<Patient> patients = new ArrayList<Patient>();
    
    		if(authenticDoctor != null) {
        		for (String nID : authenticDoctor.getPatientID()){
    
        			Patient patient = dbserviceClient.getPatientByNId(nID);
        			Date birthday = new GregorianCalendar(patient.getDateYear(), patient.getDateMonth()-1, patient.getDateDay()).getTime(); 
        			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
        			
        			patient.setBirthday(sdf.format(birthday.getTime()));
        			patients.add(patient);
        		}
        		model.addObject("patients",patients);
        		return model;
    		} else {
    			return model;
    		}
    		
    		
    	} else {
    		ModelAndView model = new ModelAndView("businessClients/doctor/home");
    		return model;
    	}
    	
    }
    
    @GetMapping("/askPermission")
    public ModelAndView askPermission(@RequestParam("nID") String nID, HttpSession session){
    	
    	String doctorName = (String) session.getAttribute("loggedInDoctorName");
    	String doctorEmail = (String)session.getAttribute("loggedInDoctorEmail");
    		 
    	ModelAndView model = new ModelAndView("businessClients/doctor/response");
    	
    	model.addObject("nID", nID);
		if(dbserviceClient.getPatientByNId(nID) == null) {
			model.addObject("response", "invalid");
		} else if(dbserviceClient.getPatientByNId(nID).getEmail() == null){
			model.addObject("response", "noEmail");
		} else if(dbserviceClient.isAuthenticated(nID, doctorEmail)) {
			model = new ModelAndView("redirect:/user/profile/mydrugs/"+nID);
			return model;
		} else {
			String subject = "Permission";
			String recipient = dbserviceClient.getPatientByNId(nID).getEmail();
	    	String body = "Doctor <strong>" + doctorName + "</strong> with email " + doctorEmail + "asks for your permission <br/>"
	        		+"<a href=\"http://18.191.180.56:8080/doctor/request?value=yes&doctor=" + doctorEmail +"&patient="+ nID + "\" target=\"_parent\"><button> Accept </button></a> "
	        	    +"<a href=\"http://18.191.180.56:8080/doctor/request?value=no&doctor=" + doctorEmail +"&patient="+ nID + "\" target=\"_parent\"><button> Reject </button></a> \n"
	        	    + "";
	    	
	    	EmailModel email = new EmailModel(recipient, body, subject);
			mailserviceClient.sendHTMLEmail(email);
			

			
			
			if(dbserviceClient.getMobileTokenByID(nID) != null) {
				String to = dbserviceClient.getMobileTokenByID(nID);
				PushNotification push = new PushNotification();
				push.setTo(to);
				Map<String, String> data = new HashMap<String, String>();
				String msg = "Doctor" + doctorName + " with email " + doctorEmail + "asks for your permission";
				data.put("msg", msg);
				data.put("doctorEmail", doctorEmail);
				push.setData(data);
				
				dbserviceClient.pushNotification(push);
			}
			model.addObject("response", "waiting");
		}
    	return model;
    }
    
    @GetMapping("/request")
    @ResponseBody
    public String requestHandler(@RequestParam("value") String value, @RequestParam("doctor") String doctorEmail, @RequestParam("patient") String patient_nId) {
    	
		Patient patient =  dbserviceClient.getPatientByNId(patient_nId);
		String recipient = doctorEmail;
    	String subject = "Patient Request";
    	AuthenticDoctor authenticDoctor = new AuthenticDoctor();
    	
    	if(value.equals("yes")) {
    		
        	
    		ArrayList<String> doctorEmails = new ArrayList<String>();
    		doctorEmails.add(doctorEmail);
    		patient.setDoctorEmail(doctorEmails);
    		dbserviceClient.updatePatientPersonalInfo(patient);
    		
    		
    		authenticDoctor = dbserviceClient.findPatiendNIDByAuthenticDoctorEmail(doctorEmail);
    		
    		if(authenticDoctor == null) {
    			authenticDoctor = new AuthenticDoctor();
    			authenticDoctor.setDoctorEmail(doctorEmail);
    			ArrayList<String> patientIDs = new ArrayList<String>();
    			patientIDs.add(patient_nId);
    			authenticDoctor.setPatientID(patientIDs);
    		} else {
    			authenticDoctor.setDoctorEmail(doctorEmail);
    			ArrayList<String> patientIDs = authenticDoctor.getPatientID();
    			patientIDs.add(patient_nId);
    			authenticDoctor.setPatientID(patientIDs);
    		}
    		
    		dbserviceClient.addAuthenticatedDoctorEmail(authenticDoctor);
    		String body = "Patient " + patient.getFirstName() + " " + patient.getLastName() + " has accepted your request, you can show the patient profile now";
        	EmailModel email = new EmailModel(recipient, body, subject);
        	mailserviceClient.sendSimpleEmail(email);
        	
        	return "Request is accepted, Doctor with eamil" + doctorEmail +"has recieved a confirmation email";
    	} else {
    		String body = "Patient " + patient.getFirstName() + " " + patient.getLastName() + " has rejected your request"; 
    		EmailModel email = new EmailModel(recipient, body, subject);
        	mailserviceClient.sendSimpleEmail(email);
    		return "Request is rejected, Doctor with email" + doctorEmail + "has recieved a rejection email";
    	}
    }
    
}
