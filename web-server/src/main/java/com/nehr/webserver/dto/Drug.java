package com.nehr.webserver.dto;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Drug {
	private String id;
	private String name;
	private String description;
	private List<String> diseases;
	private List<String> effectiveSubstance;
	private List<String> symptoms;
	private Boolean active;
	private String image;
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getDiseases() {
		return diseases;
	}
	public void setDiseases(List<String> diseases) {
		this.diseases = diseases;
	}
	public List<String> getEffectiveSubstance() {
		return effectiveSubstance;
	}
	public void setEffectiveSubstance(List<String> effectiveSubstance) {
		this.effectiveSubstance = effectiveSubstance;
	}
	public List<String> getSymptoms() {
		return symptoms;
	}
	public void setSymptoms(List<String> symptoms) {
		this.symptoms = symptoms;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	

}
