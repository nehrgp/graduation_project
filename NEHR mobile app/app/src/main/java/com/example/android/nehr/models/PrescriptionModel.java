package com.example.android.nehr.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)

public class PrescriptionModel implements Serializable {

    private String doctorName ;
    private String prescriptionDate ;
    private String  prescriptionImage ;

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPrescriptionDate() {
        return prescriptionDate;
    }

    public void setPrescriptionDate(String prescriptionDate) {
        this.prescriptionDate = prescriptionDate;
    }

    public String  getPrescriptionImage() {
        return prescriptionImage;
    }

    public void setPrescriptionImage(String  prescriptionImage) {
        this.prescriptionImage = prescriptionImage;
    }
}
