package com.example.android.nehr.data;

import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.models.MobileInstance;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.presenters.DrugSearchPresenter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.internal.operators.observable.ObservableEmpty;


/**
 * Created by mahmoud on 2/7/2018.
 */

public interface DBClient {

    public Observable<LoginModel> getBasicInfoByMobileNumber(String mobileNumber);
    public Observable<SignupModel> upsertBasicInfo(SignupModel signupModel);
    public Observable<List<DrugModel>> getDrugsByPrefix(String prefix);
    public Observable<PatientDrugHistory> addDrugToPatientDrugHistory(PatientDrugHistory patientDrugHistory);
    public Observable<List<DrugHistory>> getPatientDrugs(String id);
    public Observable<MobileInstance>  saveInstanceIdToken(MobileInstance instance);
    public Observable<String>  sendAuthorizationResponse(String response,String doctorEmail,String patientNID);



}
