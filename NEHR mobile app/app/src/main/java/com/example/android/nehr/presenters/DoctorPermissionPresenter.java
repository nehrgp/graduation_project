package com.example.android.nehr.presenters;

public interface DoctorPermissionPresenter {
        void sendAuthorizationResponse(String response,String doctorEmail,String patientNID);
}
