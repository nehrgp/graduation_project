package com.nehr.dbservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.DrugCrudDao;
import com.nehr.dbservice.models.Drug;

@RestController
public class DrugCrudController {
	@Autowired 
	DrugCrudDao drugCrudDao;
	
    @RequestMapping(value="/drug/upsert",method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Drug upsertDrug(@RequestBody Drug drug){
    	return drugCrudDao.upsertDrug(drug);
    }
    
    @RequestMapping(value="/drug/delete/{id}",method=RequestMethod.DELETE)
    public void deleteDrugById(@PathVariable("id") String id){
    	 drugCrudDao.deleteDrugById(id);
    	 		 
    }
    
    @RequestMapping(value="/drug/find/id/{id}",method=RequestMethod.GET)
    public Drug findDrugById(@PathVariable("id") String id){
    	 return drugCrudDao.findDrugById(id);
    }
    
    @RequestMapping(value="/drug/find/name/{name}",method=RequestMethod.GET)
    public Drug findDrugByName(@PathVariable("name") String name){
    	 return drugCrudDao.findDrugByName(name);
    }
    
    @RequestMapping(value="/drug/find/all")
    public List<Drug> findAllDrugs(){
    	return drugCrudDao.findAll();
    }
    
}
