package com.nehr.dbservice.dao;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.nehr.dbservice.models.EEGStreamModel;

@Repository
public class EEGDaoImpl implements EEGDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;
	static final int numberOFEEGChannels = 8;
	
	@Override
	public EEGStreamModel upsertData(EEGStreamModel eegModel) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		
		EEGStreamModel updatedEEGModel = this.getDataById(eegModel.getId());
		
		if(updatedEEGModel != null) {
			for(int i=0; i<numberOFEEGChannels; i++ ) {
				if(updatedEEGModel.getData().isEmpty() || updatedEEGModel.getData() == null) {
					updatedEEGModel.setData(new ArrayList<ArrayList<Float>>(numberOFEEGChannels));
				}
				
				if(i >= updatedEEGModel.getData().size()) {
					updatedEEGModel.getData().add(new ArrayList<Float>());
				}
			
				updatedEEGModel.getData().get(i).addAll(eegModel.getData().get(i));
			}
		} else {
			updatedEEGModel = eegModel;
		}
		
		try {
			mapper.save(updatedEEGModel, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return this.getDataById(updatedEEGModel.getId());
		}
		
	}

	@Override
	public EEGStreamModel getDataById(String id) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		EEGStreamModel eegModel = new EEGStreamModel();
		eegModel.setId(id);
		EEGStreamModel result = null;
		try {
			result = mapper.load(eegModel);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;
		}
	}

}
