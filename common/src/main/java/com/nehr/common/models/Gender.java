package com.nehr.common.models;

public enum Gender {
	MALE, FEMALE;
	private Gender() {
	}

}
