package com.nehr.webserver.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class SignupBusinessModel {
	@NotEmpty
	String businessClientType;
	@NotEmpty
	String userName;
	@NotEmpty
	String email;
	@NotEmpty
	String password;
	@NotEmpty
	String passwordRepeat;

	public SignupBusinessModel(){};
	public SignupBusinessModel(	String businessClientType, String userName, String email, String password, 	String passwordRepeat) {
		super();
		this.userName = userName;
		this.password = password;
		this.businessClientType = businessClientType;
		this.passwordRepeat = passwordRepeat;
		this.email = email;
	}
	
	

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBusinessClientType() {
		return businessClientType;
	}
	public void setBusinessClientType(String businessClientType) {
		this.businessClientType = businessClientType;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordRepeat() {
		return passwordRepeat;
	}
	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

}
