package com.nehr.dbservice.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.SaveBehavior;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.nehr.dbservice.models.Patient;

@Repository
public class PatientDaoImpl implements PatientDao {

	@Autowired
	AmazonDynamoDBClient amazonDynamoDbClient;

	@Override
	public Patient findUserByNationalId(String nID) {

		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);
		Patient user = new Patient();
		user.setnID(nID);
		Patient result = null;
		try {
			result = mapper.load(user);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;

		}

	}

	@Override
	public Patient findUserByMobileNumber(String mobileNumber) {

		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		HashMap<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":v1", new AttributeValue().withS(mobileNumber));

		DynamoDBQueryExpression<Patient> queryExpression = new DynamoDBQueryExpression<Patient>()
				.withIndexName("mobileNumber-index").withConsistentRead(false)
				.withKeyConditionExpression("mobileNumber = :v1").withExpressionAttributeValues(eav);
		List<Patient> iList = mapper.query(Patient.class, queryExpression);
		if (iList.isEmpty()) {
			return null;
		} else
			return iList.get(0);

	}

	@Override
	public Patient upsertUser(Patient user) {
		DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDbClient);

		try {
			mapper.save(user, new DynamoDBMapperConfig(SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES));
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			return this.findUserByNationalId(user.getnID());

		}

	}

}
