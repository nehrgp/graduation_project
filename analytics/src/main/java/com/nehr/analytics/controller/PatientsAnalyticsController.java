package com.nehr.analytics.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.analytics.models.PatientSmokersByGender;
import com.nehr.analytics.models.PatientsDividedByAge;
import com.nehr.analytics.service.PatientAnalytics;


@RestController
public class PatientsAnalyticsController {
	
	@Autowired
	PatientAnalytics patientAnalytics;
	

	@RequestMapping(value = "/analytics/patientsSlides/age", method = RequestMethod.GET)
	public Map<String, Integer> getSlidesByAge() {
		return patientAnalytics.getPatientsSlides();
	}
	

	@RequestMapping(value = "/analytics/patientsSlides/smokers/gender", method = RequestMethod.GET)
	public Map<String, Integer> getSmokersSlidesByGender() {
		return patientAnalytics.getSmokersPercentages();
	}
	
	

}
