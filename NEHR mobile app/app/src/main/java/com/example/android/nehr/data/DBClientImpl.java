package com.example.android.nehr.data;

import android.util.Log;

import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.models.MobileInstance;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.models.SignupModel;

import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.internal.operators.observable.ObservableEmpty;

/**
 * Created by mahmoud on 2/8/2018.
 */

public class DBClientImpl implements DBClient {

    RemoteDBClient remoteClient;
    LocalDBClient localClient;

    public DBClientImpl() {
        remoteClient = new RemoteDBClient();
        localClient = new LocalDBClient();

    }

    @Override
    public Observable<LoginModel> getBasicInfoByMobileNumber(String mobileNumber) {
        return Observable.defer(()-> Observable.just(remoteClient.getBasicInfoByMobileNumber(mobileNumber)));

    }

    @Override
    public Observable<SignupModel> upsertBasicInfo(SignupModel signupModel) {

        return Observable.defer(()-> Observable.just(remoteClient.upsertBasicInfo(signupModel)));
    }

    @Override
    public Observable<List<DrugModel>> getDrugsByPrefix(String prefix) {
        return Observable.defer(()-> Observable.just(remoteClient.getDrugsByPrefix(prefix)));
    }

    @Override
    public Observable<PatientDrugHistory> addDrugToPatientDrugHistory(PatientDrugHistory patientDrugHistory) {
        return Observable.defer(()-> Observable.just(remoteClient.addToPatientDrugHistory(patientDrugHistory)));
    }

    @Override
    public Observable<List<DrugHistory>> getPatientDrugs(String id) {
        return Observable.defer(()-> Observable.just(remoteClient.getPatientDrugs(id)));
    }

    @Override
    public Observable<MobileInstance> saveInstanceIdToken(MobileInstance instance) {
        return ObservableEmpty.defer(()-> Observable.just(remoteClient.saveToken(instance)));

    }

    @Override
    public Observable<String> sendAuthorizationResponse(String response, String doctorEmail, String patientNID) {
        return Observable.defer(()-> Observable.just(remoteClient.sendAuthorizationResponse(response,doctorEmail,patientNID)));

    }


}
