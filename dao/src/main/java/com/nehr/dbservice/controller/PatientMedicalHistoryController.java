package com.nehr.dbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.PatientMedicalHistoryDao;
import com.nehr.dbservice.models.PatientMedicalHistory;

@RestController
@RequestMapping("/patient")
public class PatientMedicalHistoryController {

	@Autowired
	PatientMedicalHistoryDao patientMedicalHistoryDao;
	
    @RequestMapping(value="/patientMedicalHistory/upsert",method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public PatientMedicalHistory upsert(@RequestBody PatientMedicalHistory patientMedicalHistory) {
    	return patientMedicalHistoryDao.findPatientMedicalHistoryByNID(patientMedicalHistory.getnID());
	}
    
    @RequestMapping(value="/patientMedicalHistory/find/{nID}",method=RequestMethod.GET)
	public PatientMedicalHistory findByNID(@PathVariable("nID") String nID) {
    	return patientMedicalHistoryDao.findPatientMedicalHistoryByNID(nID);
    }
}
