package com.nehr.fileservice.dto;

import org.springframework.web.multipart.MultipartFile;

public class Prescription {

	private String doctor_name;
	private String notes;
	private String patient_nId;
	private MultipartFile image;
	private String date;
	
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getPatient_nId() {
		return patient_nId;
	}
	public void setPatient_nId(String patient_nId) {
		this.patient_nId = patient_nId;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

}
