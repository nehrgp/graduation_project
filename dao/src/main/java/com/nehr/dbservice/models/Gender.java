package com.nehr.dbservice.models;

public enum Gender {
	MALE, FEMALE;
	private Gender() {
	}

}
