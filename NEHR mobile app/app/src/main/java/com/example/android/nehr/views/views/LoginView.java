package com.example.android.nehr.views.views;


import com.example.android.nehr.models.LoginModel;


/**
 * Created by omar on 1/30/2018.
 */

public interface LoginView extends BaseView {
    void loginFailed() ;
    void loginSuccess(LoginModel loginModel) ;
}
