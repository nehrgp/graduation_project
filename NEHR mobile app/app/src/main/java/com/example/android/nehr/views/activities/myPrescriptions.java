package com.example.android.nehr.views.activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.models.PrescriptionModel;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.adapters.PrescriptionAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class myPrescriptions extends NavigationDrawer {

    @BindView(R.id.addPrescriptionBtn) FloatingActionButton addPrescription ;
    @BindView(R.id.myPrescriptionsListView) ListView myPrescriptionListView ;
    PrescriptionModel prescriptionModel = new PrescriptionModel();
    ArrayList<PrescriptionModel> prescriptionModelsList = new ArrayList<PrescriptionModel>() ;
    Gson gson = new Gson() ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_prescriptions);
        ButterKnife.bind(this);
        if(SaveSharedPreference.getPresList(getApplicationContext()).length() != 0){
            String prescriptionModelsListJson = SaveSharedPreference.getPresList(getApplicationContext());
            prescriptionModelsList = gson.fromJson(prescriptionModelsListJson, new TypeToken<List<PrescriptionModel>>() {
            }.getType());
            showPrescriptions(prescriptionModelsList);
        }

    }
    @OnClick(R.id.addPrescriptionBtn)
    public void addPrescriptionBtnClicked(){
            Intent i = new Intent(myPrescriptions.this, prescriptionForm.class);
            startActivity(i);
            finish();
        }


    public void showPrescriptions(List<PrescriptionModel> prescriptionModels) {

        PrescriptionAdapter adapter = new PrescriptionAdapter(this,prescriptionModels);
        myPrescriptionListView.setAdapter(adapter);
        myPrescriptionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),prescriptionDetails.class);
                prescriptionModel.setDoctorName(adapter.getItem(position).getDoctorName());
                prescriptionModel.setPrescriptionDate(adapter.getItem(position).getPrescriptionDate());
                prescriptionModel.setPrescriptionImage(adapter.getItem(position).getPrescriptionImage());
                intent.putExtra("prescriptionModel",prescriptionModel);
                //startActivity(intent);
            }
        });

    }

}
