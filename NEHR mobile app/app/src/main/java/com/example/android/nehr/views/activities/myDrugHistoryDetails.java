package com.example.android.nehr.views.activities;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.Services.DownloadImageService;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.presenters.MyDrugHistoryDetailsPresenter;
import com.example.android.nehr.presenters.MyDrugHistoryDetailsPresenterImpl;
import com.example.android.nehr.views.views.MyDrugsDetailsView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class myDrugHistoryDetails extends NavigationDrawer implements MyDrugsDetailsView {

    @BindView(R.id.name)
    TextView drugName;
    @BindView(R.id.startDate)
    TextView startDate;
    @BindView(R.id.endDate)
    TextView endDate;
    @BindView(R.id.drugImage)
    ImageView drugImage ;

    MyDrugHistoryDetailsPresenter presenter = new MyDrugHistoryDetailsPresenterImpl(this);
    boolean editEnabled = false;
    DrugHistory drugHistory;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private DatePickerDialog.OnDateSetListener mDateSetListener1;
    final static int RQS_1 = 4;
    private PendingIntent pendingIntent;

    String image ="" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_drug_history_details);
        ButterKnife.bind(this);
        this.showDrug(this.getDrugModelFromIntent(this.getIntent()));
        drugHistory = new DrugHistory();
        image += this.getDrugModelFromIntent(this.getIntent()).getImage() ;
        //image +="https://s3.us-east-2.amazonaws.com/s3nehr/drugs/Augmentin.jpg";
        Log.e("image",image);
        new DownloadImageService(drugImage).execute(image);

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        myDrugHistoryDetails.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                startDate.setText(date);
                save();
            }
        };
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        myDrugHistoryDetails.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                endDate.setText(date);
                save();
            }
        };


    }


    // @OnClick(R.id.editBtn)
    public void save() {
        drugHistory.setName(getDrugModelFromIntent(this.getIntent()).getName());
        drugHistory.setStartDate(String.valueOf(startDate.getText()));
        drugHistory.setEndDate(String.valueOf(endDate.getText()));
        presenter.addToMyDrugs(drugHistory);
    }

    @Override
    public void drugShowSuccess() {
        Toast.makeText(this.getApplicationContext(), "success", Toast.LENGTH_LONG).show();
    }

    @Override
    public void drugShowFailed() {
        Toast.makeText(this.getApplicationContext(), "fail", Toast.LENGTH_LONG).show();
    }

    private DrugHistory getDrugModelFromIntent(Intent intent) {
        return (DrugHistory) intent.getSerializableExtra("drugHistory");
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    public void showDrug(DrugHistory drugHistory) {
        drugName.setText(drugHistory.getName());
        startDate.setText(drugHistory.getStartDate());
        endDate.setText(drugHistory.getEndDate());
       // new DownloadImageService(drugImage).execute(image);

    }

}
