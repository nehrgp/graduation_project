package com.nehr.dbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.EEGDao;
import com.nehr.dbservice.models.EEGStreamModel;

@RestController
@RequestMapping("/eeg")
public class EEGController {

	@Autowired
	EEGDao eegDao;
	
	@GetMapping("/get/{id}")
	public EEGStreamModel getDataById(@PathVariable("id") String id) {
		return eegDao.getDataById(id);
	}
	
	@PostMapping("/add")
	public EEGStreamModel upsertData(@RequestBody EEGStreamModel eegModel) {
		return eegDao.upsertData(eegModel);
	}

}
