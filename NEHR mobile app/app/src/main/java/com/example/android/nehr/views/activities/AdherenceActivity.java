package com.example.android.nehr.views.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.alarm.AlarmCursorAdapter;
import com.example.android.nehr.alarm.data.AlarmReminderContract;
import com.example.android.nehr.alarm.data.AlarmReminderDbHelper;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.presenters.AlarmDrugPresenterImpl;
import com.example.android.nehr.presenters.DrugPresenter;
import com.example.android.nehr.views.adapters.AdhereneAdapter;
import com.example.android.nehr.views.views.AlarmView;

import java.util.ArrayList;
import java.util.List;

public class AdherenceActivity extends NavigationDrawer  implements LoaderManager.LoaderCallbacks<Cursor> , AlarmView {
    AdhereneAdapter mCursorAdapter;
    AlarmReminderDbHelper alarmReminderDbHelper = new AlarmReminderDbHelper(this);
    ListView reminderListView;
    ProgressDialog prgDialog;
    DrugPresenter presenter = new AlarmDrugPresenterImpl(this);
    String selectedDrugName;

    private String alarmTitle = "";

    private static final int VEHICLE_LOADER = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adherence);


        reminderListView = (ListView) findViewById(R.id.list);


        View emptyView = findViewById(R.id.empty_view);
        reminderListView.setEmptyView(emptyView);

        mCursorAdapter = new AdhereneAdapter(this, null);
        reminderListView.setAdapter(mCursorAdapter);


        getSupportLoaderManager().initLoader(VEHICLE_LOADER, null, this);


    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                AlarmReminderContract.AlarmReminderEntry._ID,
                AlarmReminderContract.AlarmReminderEntry.KEY_TITLE,
                AlarmReminderContract.AlarmReminderEntry.KEY_DATE,
                AlarmReminderContract.AlarmReminderEntry.KEY_TIME,
                AlarmReminderContract.AlarmReminderEntry.KEY_REPEAT,
                AlarmReminderContract.AlarmReminderEntry.KEY_REPEAT_NO,
                AlarmReminderContract.AlarmReminderEntry.KEY_REPEAT_TYPE,
                AlarmReminderContract.AlarmReminderEntry.KEY_ACTIVE

        };

        return new CursorLoader(this,   // Parent activity context
                AlarmReminderContract.AlarmReminderEntry.CONTENT_URI,   // Provider content URI to query
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mCursorAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);

    }



    public void restartLoader(){
        getSupportLoaderManager().restartLoader(VEHICLE_LOADER, null, this);
    }

    @Override
    public void showDrugs(List<DrugHistory> drugHistory) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Select Drug");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_singlechoice);
        List<String> drugsNames=new ArrayList<>();
        for(int i=0;i<drugHistory.size();i++){
            drugsNames.add(drugHistory.get(i).getName());
            arrayAdapter.add(drugsNames.get(i));
        }





        // android.app.AlertDialog alert = builder.create();
        // alert.show();
    }

    @Override
    public void fail() {

    }

    @Override
    public Context getContext() {
        return this;
    }


}
