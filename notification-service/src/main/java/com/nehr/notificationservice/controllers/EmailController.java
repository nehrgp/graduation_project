package com.nehr.notificationservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.notificationservice.email.EmailService;
import com.nehr.notificationservice.models.EmailModel;

@RestController
@RequestMapping("/email")
public class EmailController {
	@Autowired
	EmailService emailService;

	@RequestMapping(value = "/send/simpleEmail", method = RequestMethod.POST)
	public void sendSimpleEmail(@RequestBody EmailModel simpleEmailModel) {
		emailService.sendSimpleMessage(simpleEmailModel);
	}

	@RequestMapping(value = "/send/htmlEmail", method = RequestMethod.POST)
	public void sendHtmlEmail(@RequestBody EmailModel emailModel) {
		emailService.sendHtmlMessageUsingTemplate(emailModel);
	}

}
