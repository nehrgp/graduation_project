package com.nehr.webserver.dto;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthenticDoctor {
	
	String doctorEmail;
	ArrayList<String> patientID;
	
	public AuthenticDoctor(){}
	
	public AuthenticDoctor(@JsonProperty("doctorEmail") String doctorEmail,@JsonProperty("patientID") ArrayList<String> patientID) {
		super();
		this.patientID = patientID;
		this.doctorEmail = doctorEmail;
	}

	public String getDoctorEmail() {
		return doctorEmail;
	}

	public void setDoctorEmail(String doctorEmail) {
		this.doctorEmail = doctorEmail;
	}

	public ArrayList<String> getPatientID() {
		return patientID;
	}

	public void setPatientID(ArrayList<String> patientID) {
		this.patientID = patientID;
	}
	

}
