package com.example.android.nehr.presenters;


import com.example.android.nehr.models.LoginModel;

/**
 * Created by omar on 1/30/2018.
 */

public interface LoginPresenter {

    void attmeptLogin(LoginModel loginModel);
    void loginSuccess(LoginModel loginModel);
    void loginFailure();
}
