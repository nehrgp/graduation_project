package com.nehr.dbservice.models;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class DrugHistory {
	private String name;
	private String startDate;
	private String endDate;
	private String image;
	
	@DynamoDBAttribute(attributeName = "image")
    public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@DynamoDBAttribute(attributeName = "drugName")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


    @DynamoDBAttribute(attributeName = "startDate")
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    @DynamoDBAttribute(attributeName = "endDate")
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
