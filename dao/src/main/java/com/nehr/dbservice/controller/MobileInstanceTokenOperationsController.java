package com.nehr.dbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nehr.dbservice.dao.MobileInstanceDao;
import com.nehr.dbservice.models.MobileInstance;

@RestController
@RequestMapping("/mobileInstance/token")
public class MobileInstanceTokenOperationsController {

	@Autowired
	MobileInstanceDao mobileInstanceDao;

	@RequestMapping(value = "/get/nId/{nId}", method = RequestMethod.GET)
	public String getInstanceIdToken(@PathVariable("nId") String nId) {
		
			return mobileInstanceDao.getMobileInstance(nId).getMobileInstanceIdToken();
		
	}

	@RequestMapping(value = "/set", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void upsertPatient(@RequestBody MobileInstance mobileInstance) {

		mobileInstanceDao.setMobileInstanceId(mobileInstance.getnId(), mobileInstance.getMobileInstanceIdToken());


	
}

}
