package com.example.android.nehr.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.alarm.AlarmMainActivity;
import com.example.android.nehr.sessions.SaveSharedPreference;


public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(int layoutResID) {


        final DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_navigation_drawer, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.screen_area);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);






    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(getApplicationContext(),profileActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_myDrugs) {
            Intent intent = new Intent(getApplicationContext(),myDrugsActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_searchDrugs) {
            Intent intent = new Intent(getApplicationContext(),alldrugsActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_alarms) {
            Intent intent = new Intent(getApplicationContext(),AlarmMainActivity.class);
            startActivity(intent);
            finish();

        }else if (id == R.id.nav_adherence) {
            Intent intent = new Intent(getApplicationContext(),AdherenceActivity.class);
            startActivity(intent);
            finish();

        }else if (id == R.id.nav_logout) {

            SaveSharedPreference.logout(getApplicationContext());
            Intent i = new Intent(getApplicationContext(), startActivity.class);
            startActivity(i);
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            finish();
            this.startActivity(new Intent(this,AlarmMainActivity.class));
        }
        return true;
    }


}
