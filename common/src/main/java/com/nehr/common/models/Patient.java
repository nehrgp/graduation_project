package com.nehr.common.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Patient {

	@Id
	String id;

	@Field
	String firstName;

	@Field
	String email;

	@Field
	String lastName;

	@Field
	int dateDay;

	@Field
	int dateMonth;

	@Field
	int dateYear;

	@Field
	String gender;

	@Field
	String mobileNumber;

	@Field
	String passwordHash;

	@PersistenceConstructor
	public Patient(@JsonProperty("id") String id, @JsonProperty("firstName") String firstName,
			@JsonProperty("lastName") String lastName, @JsonProperty("dateDay") int dateDay,
			@JsonProperty("dateMonth") int dateMonth, @JsonProperty("dateYear") int dateYear,
			@JsonProperty("gender") String gender, @JsonProperty("mobileNumber") String mobileNumber,
			@JsonProperty("passwordHash") String passwordHash, @JsonProperty("email") String email) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateDay=dateDay;
		this.dateMonth=dateMonth;
		this.dateYear=dateYear;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.passwordHash = passwordHash;
		this.email = email;
	}

	
	public int getDateDay() {
		return dateDay;
	}


	public void setDateDay(int dateDay) {
		this.dateDay = dateDay;
	}


	public int getDateMonth() {
		return dateMonth;
	}


	public void setDateMonth(int dateMonth) {
		this.dateMonth = dateMonth;
	}


	public int getDateYear() {
		return dateYear;
	}


	public void setDateYear(int dateYear) {
		this.dateYear = dateYear;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
