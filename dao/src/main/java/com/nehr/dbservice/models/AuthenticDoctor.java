package com.nehr.dbservice.models;

import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonProperty;

@DynamoDBTable(tableName = "Authorization")
public class AuthenticDoctor {

	String doctorEmail;
	ArrayList<String> patientID;
	
	public AuthenticDoctor() {}
	
	public AuthenticDoctor(@JsonProperty("doctorEmail") String doctorEmail, @JsonProperty("patientID")ArrayList<String> patientID) {
		super();
		this.patientID = patientID;
		this.doctorEmail = doctorEmail;
	}

	@DynamoDBHashKey(attributeName = "doctorEmail")
	public String getDoctorEmail() {
		return doctorEmail;
	}

	public void setDoctorEmail(String doctorEmail) {
		this.doctorEmail = doctorEmail;
	}

	@DynamoDBAttribute(attributeName = "patientID")
	public ArrayList<String> getPatientID() {
		return patientID;
	}

	public void setPatientID(ArrayList<String> patientID) {
		this.patientID = patientID;
	}
	
}
