package com.nehr.fileservice.dto;

import org.springframework.web.multipart.MultipartFile;

public class PrescriptionDao {

	private String doctor_name;
	private String notes;
	private String patient_nId;
	private String date;
	private String url;
	
	public PrescriptionDao() {
		
	}
	
	public PrescriptionDao(Prescription prescription) {
		this.doctor_name = prescription.getDoctor_name();
		this.notes = prescription.getNotes();
		this.patient_nId = prescription.getPatient_nId();
		this.date = prescription.getDate();
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getPatient_nId() {
		return patient_nId;
	}
	public void setPatient_nId(String patient_nId) {
		this.patient_nId = patient_nId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

}
