package com.nehr.notificationservice.email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.nehr.notificationservice.models.EmailModel;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	public JavaMailSender emailSender;

	@Async
	@Override
	public void sendSimpleMessage(EmailModel emailModel) {

		try{
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(emailModel.getRecipient());
		message.setSubject(emailModel.getSubject());
		message.setText(emailModel.getBody());
		emailSender.send(message);
		}catch(Exception e ){
			//TODO add proper exception handling
		}
	}

	@Async
	@Override
	public void sendHtmlMessageUsingTemplate(EmailModel emailModel) {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(emailModel.getRecipient());
			helper.setSubject(emailModel.getSubject());
			helper.setText(emailModel.getBody(), true);
			emailSender.send(message);

		} catch (MessagingException e) {
			//TODO add proper exception handling
		}

	}

}