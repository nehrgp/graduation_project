package com.example.android.nehr.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.example.android.nehr.R;
import com.example.android.nehr.alarm.data.AlarmReminderContract;
import com.example.android.nehr.sessions.SaveSharedPreference;

public class AdhereneAdapter  extends CursorAdapter {


    private TextView mTitleText, mAdherenceNumberText;
    private ImageView  mThumbnailImage;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;

    public AdhereneAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.adherence_items, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        mTitleText = (TextView) view.findViewById(R.id.recycle_title);
        mAdherenceNumberText = (TextView) view.findViewById(R.id.recycle_number);
        mThumbnailImage = (ImageView) view.findViewById(R.id.thumbnail_image);

        int titleColumnIndex = cursor.getColumnIndex(AlarmReminderContract.AlarmReminderEntry.KEY_TITLE);
        String title = cursor.getString(titleColumnIndex);
        Log.d("adherence",title);
        Integer mAdherenceNumberNum= Integer.parseInt(SaveSharedPreference.getAdherence(context,title+"Num"));
        Integer mAdherenceNumberDen= Integer.parseInt(SaveSharedPreference.getAdherence(context,title+"Den"));

        setReminderTitle(title);



            mAdherenceNumberText.setText(mAdherenceNumberNum.toString()+" out of "+mAdherenceNumberDen.toString()+" times");
        }


    // Set reminder title view
    public void setReminderTitle(String title) {
        mTitleText.setText(title);
        String letter = "A";

        if(title != null && !title.isEmpty()) {
            letter = title.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();

        // Create a circular icon consisting of  a random background colour and first letter of title
        mDrawableBuilder = TextDrawable.builder()
                .buildRound(letter, color);
        mThumbnailImage.setImageDrawable(mDrawableBuilder);
    }



}
