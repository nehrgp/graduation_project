package com.nehr.webserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nehr.webserver.clients.DbserviceClient;
import com.nehr.webserver.dto.BusinessClient;
import com.nehr.webserver.dto.BusinessClientType;
import com.nehr.webserver.dto.Patient;
import com.nehr.webserver.dto.SignupBusinessModel;
import com.nehr.webserver.dto.SignupModel;


@Controller
public class SignupController {

	@Autowired
	DbserviceClient dbserviceClient;

	@RequestMapping(value = "/user/signup", method = RequestMethod.GET)
	public String signUpPage() {
		return "signup";
	}

	@RequestMapping(value = "/user/signup", method = RequestMethod.POST)
	public String attemptSignup(SignupModel signupModel) {

		Patient patient = dbserviceClient.getPatientByMobile(signupModel.getMobileNumber());

		if (patient != null) {
			return "already-registered";
		} else if (signupModel.getPassword().equals(signupModel.getPasswordRepeat())) {
			patient = new Patient(signupModel.getnID(), null, null, 0, 0, 0, null, signupModel.getMobileNumber(),
					signupModel.getPassword(), null, null);
			dbserviceClient.updatePatientPersonalInfo(patient);
			return "login";
		} else {
			return "passwords-don't-match";
		}

	}
	
	@RequestMapping(value = "/user/signupBusiness", method = RequestMethod.GET)
	public String signupBusinessPage() {
		return "signupBusiness";
	}

	
	@RequestMapping(value = "/user/signupBusiness", method = RequestMethod.POST)
	public String attemptSignupBusiness(SignupBusinessModel signupBusinessModel) {
		
		BusinessClient client = dbserviceClient.getBusinessClientByemail(signupBusinessModel.getEmail());
		
		if (client != null) {
			return "already-registered";
		} else if (signupBusinessModel.getPassword().equals(signupBusinessModel.getPasswordRepeat())) {
			client = new BusinessClient(signupBusinessModel.getBusinessClientType(),signupBusinessModel.getUserName(),signupBusinessModel.getEmail(),
					signupBusinessModel.getPassword());
			dbserviceClient.upsertBusinessClient(client);
			if(signupBusinessModel.getBusinessClientType().equals(BusinessClientType.DOCTOR.toString())) {
				return "businessClients/doctor/login";
			}
			else {
				return "index";
			}
		} else {
			return "passwords-don't-match";
		}
	}
}
